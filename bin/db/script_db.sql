create database jobbee_db;

use jobbee_db;
-- user

create table user_type(
	id int primary key auto_increment,
    type_name varchar(255)
);

create table user(
	id int primary key auto_increment,
    user_type int,
    email varchar(255),
    dob timestamp,
    gender char(1),
    is_active char(1),
    contact_number char(12),
    image_url varchar(255),
    create_date timestamp,
    update_date timestamp,
    password varchar(255),
    constraint FK_US_TYPE foreign key(user_type) references user_type(id)
);

create table comment(
	id int primary key auto_increment,
    contents text,
    status char(1),
    create_date timestamp,
    update_date timestamp,
    user_id int,
    constraint FK_COMMENT_USER foreign key(user_id) references user(id)
);
-- company
create table company_image(
	id int primary key auto_increment,
    image_url varchar(255)
);

create table location(
	id int primary key auto_increment,
    street varchar(255),
    lat_ varchar(255),
    long_ varchar(255),
    district varchar(255),
    city varchar(255),
    country varchar(255)
);

create table blog(
	id int primary key auto_increment,
    contents text,
    status char(1),
    create_date timestamp,
    update_date timestamp,
    comment_id int,
    constraint FK_BOLG_COMMENT foreign key(comment_id) references comment(id)
);

create table event(
	id int primary key auto_increment,
    contents text,
    status char(1),
    create_date timestamp,
    update_date timestamp,
    comment_id int,
    constraint FK_EVENT_COMMENT foreign key(comment_id) references comment(id)
);

create table company(
	id int primary key auto_increment,
    com_name varchar(255) not null,
    image_id int,
    location_id int,
    event_id int,
    description varchar(255),
    constraint FK_COM_IMG foreign key(image_id) references company_image(id),
    constraint FK_COM_LC foreign key(location_id) references location(id),    
    constraint FK_COM_EVENT foreign key(event_id) references event(id)     
);


-- job

create table job_category(
	id int primary key auto_increment,
    category_name varchar(255)
);

create table job_type(
	id int primary key auto_increment,
    type_name varchar(255)
);

create table job_post(
	id int primary key auto_increment,
	job_title varchar(255),
    salary double,
    is_active char(1),
    job_description text,
    create_date timestamp,
    expired_date timestamp,
    job_type_id int,
    category_name_id int,
    company_id int,
    user_id int,
    constraint FK_JOB_TYPE foreign key(job_type_id) references job_type(id),
    constraint FK_JOB_LIST foreign key(category_name_id) references job_category(id),    
    constraint FK_JOB_COM foreign key(company_id) references company(id), 
    constraint FK_JOB_USER foreign key(user_id) references user(id) 
);

create table package(
	id int primary key auto_increment,
    price double,
    expired_date timestamp,
    range_post int,
    title varchar(255),
    description text
);

create table order_detail(
	id int primary key auto_increment,
    user_id int,
    package_id int,
    order_date timestamp,
    constraint FK_OD_USER foreign key(user_id) references user(id),
    constraint FK_OD_PK foreign key(package_id) references package(id)
);


create table employeer_profile(
	id int primary key auto_increment,
    user_id int,
    company_id int,
    constraint FK_EM_USER foreign key(user_id) references user(id),
    constraint FK_EM_COM foreign key(company_id) references company(id)
);

-- profile
create table seeker_profile(
	id int primary key auto_increment,
    user_id int,
    description text,
    constraint FK_SK_USER foreign key(user_id) references user(id) 
);

create table seeker_salary(
	id int primary key auto_increment,
    current_salary double,
    currency char(10),
    is_annually_month char(1),
    profile_id int,
    constraint FK_PR_SA foreign key(profile_id) references seeker_profile(id) 
);

create table seeker_education(
	id int primary key auto_increment,
    certicicate_name varchar(255),
    major varchar(255),
    institute_university_name varchar(255),
    start_date timestamp,
    complete_date timestamp,
    profile_id int,
    constraint FK_PR_ED foreign key(profile_id) references seeker_profile(id) 
);

create table seeker_experience(
	id int primary key auto_increment,
    is_currnt_job char(1),
    job_title varchar(255),
    company_name varchar(255),
    company_detail varchar(255),
    description varchar(255),
    start_date timestamp,
    complete_date timestamp,
	profile_id int,
    constraint FK_PR_EX foreign key(profile_id) references seeker_profile(id) 
);

create table category_interested(
	id int primary key auto_increment,
    category_name varchar(255),
	profile_id int,
    constraint FK_PR_INT foreign key(profile_id) references seeker_profile(id) 
);



create table job_activity(
	id int primary key auto_increment,
    jpb_post_id int,
    user_id int,
    apply_date timestamp,
    constraint FK_JOB_AP foreign key(jpb_post_id) references job_post(id),
    constraint FK_JOB_SK foreign key(user_id) references user(id)
);

 create table contact(
	 id int primary key auto_increment,
	 user_id int,
	 apply_date timestamp,
	 content text,
	 constraint FK_CON_US foreign key(user_id) references user(id)
 );
