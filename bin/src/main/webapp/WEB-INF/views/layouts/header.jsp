<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>

	<!-- Basic Page Needs
================================================== -->
	<meta charset="utf-8">
	<title>Jobbee</title>

	<!-- Mobile Specific Metas
================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS
================================================== -->
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/colors/green.css" id="colors">

	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>
<div id="wrapper">
	<header>
			<div class="container">
				<div class="sixteen columns">

					<!-- Logo -->
					<div id="logo">
						<h1><a href="index.html"><img src="images/logo-01.png" alt="Jobbee" /></a></h1>
					</div>

					<!-- Menu -->
					<nav id="navigation" class="menu">
						<ul id="responsive">

							<li><a href="index.html" id="current">Home</a>
								<ul>
									<li><a href="index.html">Home #1</a></li>
									<li><a href="index-2.html">Home #2</a></li>
									<li><a href="index-3.html">Home #3</a></li>
									<li><a href="index-4.html">Home #4</a></li>
								</ul>
							</li>

							<li><a href="#">Pages</a>
								<ul>
									<li><a href='<c:url value="/job-page"/>'>Job Page</a></li>
									<li><a href="job-page-alt.html">Job Page Alternative</a></li>
									<li><a href="resume-page.html">Resume Page</a></li>
									<li><a href="shortcodes.html">Shortcodes</a></li>
									<li><a href="pricing-tables.html">Pricing Tables</a></li>
									<li><a href="contact.html">Contact</a></li>
								</ul>
							</li>

							<li><a href="#">For Candidates</a>
								<ul>
									<li><a href="browse-jobs.html">Browse Jobs</a></li>
									<li><a href="browse-categories.html">Browse Categories</a></li>
									<li><a href="add-resume.html">Add Resume</a></li>
									<li><a href="manage-resumes.html">Manage Resumes</a></li>
									<li><a href="job-alerts.html">Job Alerts</a></li>
								</ul>
							</li>

							<li><a href="#">For Employers</a>
								<ul>
									<li><a href="add-job.html">Add Job</a></li>
									<li><a href='<c:url value="/manage-post"/>'>Manage Jobs</a></li>
									<li><a href="manage-applications.html">Manage Applications</a></li>
									<li><a href="browse-resumes.html">Browse Resumes</a></li>
								</ul>
							</li>

							<li><a href="blog.html">Blog</a></li>
						</ul>


						<ul class="float-right">
							<li><a href="my-account.html#tab2"><i class="fa fa-user"></i> Sign Up</a></li>
							<li><a href="my-account.html"><i class="fa fa-lock"></i> Log In</a></li>
						</ul>

					</nav>

					<!-- Navigation -->
					<div id="mobile-navigation">
						<a href="#menu" class="menu-trigger"><i class="fa fa-reorder"></i> Menu</a>
					</div>

				</div>
			</div>
		</header>
		<div class="clearfix"></div>
</body>
</html>