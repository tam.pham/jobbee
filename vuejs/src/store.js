// import Vue from "vue";
// import Vuex from "vuex";

// import createPersistedState from "vuex-persistedstate";

// import helper from "./services/helper";
// import config from "./services/config";

// Vue.use(Vuex);

// const debug = process.env.NODE_ENV !== "production";
// export default new Vuex.Store({
//     state: {
//         layout: {
//             navPos: "left", //top, bottom, left, right, false
//             toolbar: "top", //top, bottom, false
//             footer: true, //above, below, false
//             boxed: false, //true, false
//             roundedCorners: false, //true, false
//             viewAnimation: "fade-top" // fade-left, fade-right, fade-top, fade-top-in-out, fade-bottom, fade-bottom-in-out, fade, false
//         },
//         splashScreen: true,
//         logged: helper.checkLogin(),
//         profile: helper.getLocalProfile(),
//         search: "",
//         itemPerPage: config.itemPerPage ? config.itemPerPage : 10,
//         searchAll: false,
//         searchSubmit: "",
//         searchTag: "",
//         commitHash: process.env.VUE_APP_COMMIT_HASH,
//         isLogoutClick: false
//     },
//     mutations: {
//         setLayout(state, payload) {
//             if (payload && payload.navPos !== undefined)
//                 state.layout.navPos = payload.navPos;

//             if (payload && payload.toolbar !== undefined)
//                 state.layout.toolbar = payload.toolbar;

//             if (payload && payload.footer !== undefined)
//                 state.layout.footer = payload.footer;

//             if (payload && payload.boxed !== undefined)
//                 state.layout.boxed = payload.boxed;

//             if (payload && payload.roundedCorners !== undefined)
//                 state.layout.roundedCorners = payload.roundedCorners;

//             if (payload && payload.viewAnimation !== undefined)
//                 state.layout.viewAnimation = payload.viewAnimation;
//         },
//         setLogin(state, payload) {
//             state.logged = true;
//         },
//         setLogout(state, payload) {
//             state.layout.navPos = null;
//             state.layout.toolbar = null;
//             state.logged = false;
//         },
//         setSplashScreen(state, payload) {
//             state.splashScreen = payload;
//         },
//         setProfile(state, { id, name, userName, avatar }) {
//             state.profile.id = id;
//             state.profile.name = name;
//             state.profile.userName = userName;
//             state.profile.avatar = avatar;
//         },
//         setSearchAll(state, value) {
//             // console.log('update ', state)
//             state.searchAll = value;
//         },
//         setSearchValue(state, value) {
//             state.search = value;
//         },
//         setSearchSubmit(state, value) {
//             state.searchSubmit = value;
//         },
//         setSearchTag(state, value) {
//             state.searchTag = value;
//         },
//         setOnClickLogout(state, value) {
//             state.isLogoutClick = value;
//         }
//     },
//     actions: {
//         // setSearchValue(state, value){
//         // 	// console.log(state);
//         //     state.commit('setSearchValue', value)
//         // },
//         // setSearchSubmit(state, value){
//         //     // console.log(state);
//         //     state.commit('setSearchSubmit', value)
//         // },
//         // setSearchTag(state, value){
//         //     // console.log(state);
//         //     state.commit('setSearchTag', value)
//         // },
//         // setOnClickLogout(state, value){
//         //     state.commit('setOnClickLogout', value)
//         // },
//     },
//     getters: {
//         layout(state, getters) {
//             return state.layout;
//         },
//         navPos(state, getters) {
//             return state.layout.navPos;
//         },
//         toolbar(state, getters) {
//             return state.layout.toolbar;
//         },
//         footer(state, getters) {
//             return state.layout.footer;
//         },
//         boxed(state, getters) {
//             return state.layout.boxed;
//         },
//         roundedCorners(state, getters) {
//             return state.layout.roundedCorners;
//         },
//         viewAnimation(state, getters) {
//             return state.layout.viewAnimation;
//         },
//         isLogged(state, getters) {
//             return state.logged;
//         },
//         splashScreen(state, getters) {
//             return state.splashScreen;
//         },
//         getProfile(state) {
//             return state.profile;
//         },
//         getSearchValue(state) {
//             return state.search;
//         }
//     },
//     plugins: [createPersistedState({ paths: ["layout"] })],
//     strict: debug
// });
