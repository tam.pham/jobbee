import Vue from "vue";
import Router from "vue-router";

//apps
// import Dashboard from "../views/pages/dashboard/Dashboard.vue";
import Employer from "../views/pages/employer/index.vue";
import EmployerDetail from "../views/pages/employer/detail.vue";
import EmployerEdit from "../views/pages/employer/edit.vue";
import JobSeeker from "../views/pages/job_seeker/index.vue";
import JobSeekerDetail from "../views/pages/job_seeker/detail.vue";
import JobSeekerEdit from "../views/pages/job_seeker/edit.vue";
import Job from "../views/pages/job/index.vue";
import JobDetail from "../views/pages/job/detail.vue";
import JobEdit from "../views/pages/job/edit.vue";
import Category from "../views/pages/category/index.vue";

import Account from "../views/pages/account/index.vue";
import AccountDetail from "../views/pages/account/detail.vue";
import AccountEdit from "../views/pages/account/edit.vue";

import Dashboard from "../views/pages/statistic/chart.vue";
// Blog
import Blog from "../views/pages/blog/index.vue";
import BlogCreate from "../views/pages/blog/create.vue";
import BlogEdit from "../views/pages/blog/edit.vue";
import BlogDetail from "../views/pages/blog/detail.vue";

// Blog End

//pages
import Login from "../views/pages/authentication/Login.vue";

import NotFound from "../views/pages/NotFound.vue";
import layouts from "../layout";
// import store from "../store";
import store from "../store/index";
import { auth as authAPI } from "~/services/apis/index";
import helper from "~/services/helper";

Vue.use(Router);
const router = new Router({
  mode: "history",
  routes: [
    // {
    // 	path: '/',
    // 	alias: '/dashboard',
    // 	name: 'dashboard',
    // 	component: Dashboard,
    // 	meta: {
    // 		auth: true,
    // 		layout: layouts.navLeft,
    // 		searchable: true,
    // 		tags: ['app'],
    //         navPath: '/dashboard'
    // 	}
    // },

    {
      path: "/employers",
      component: Employer,
      meta: {
        auth: true,
        layout: layouts.navLeft,
        searchable: true,
        tags: ["Employers"],
        navPath: "/employers"
      }
    },
    {
      path: "/employers",
      alias: "/employers",
      name: "employers",
      component: Employer,
      meta: {
        auth: true,
        layout: layouts.navLeft,
        searchable: true,
        tags: ["Employers"],
        navPath: "/employers"
      }
    },
    {
      path: "/employers/:id",
      name: "employers_detail",
      component: EmployerDetail,
      meta: {
        auth: true,
        layout: layouts.navLeft,
        searchable: true,
        tags: ["Detail"],
        navPath: "/employers"
      }
    },
    {
      path: "/employers/:id/edit",
      name: "employers_edit",
      component: EmployerEdit,
      meta: {
        auth: true,
        layout: layouts.navLeft,
        searchable: true,
        tags: ["Detail"],
        navPath: "/employers"
      }
    },
    {
      path: "/job-seekers",
      name: "job_seekers",
      component: JobSeeker,
      meta: {
        auth: true,
        layout: layouts.navLeft,
        searchable: true,
        tags: ["Seekers"],
        navPath: "/job-seekers"
      }
    },
    {
      path: "/job-seekers/:id",
      name: "job_seekers_detail",
      component: JobSeekerDetail,
      meta: {
        auth: true,
        layout: layouts.navLeft,
        searchable: true,
        tags: ["Detail"],
        navPath: "/job-seekers"
      }
    },
    {
      path: "/job-seekers/:id/edit",
      name: "seeker_edit",
      component: JobSeekerEdit,
      meta: {
        auth: true,
        layout: layouts.navLeft,
        searchable: true,
        tags: ["Detail"],
        navPath: "/job-seekers"
      }
    },
    {
      alias: "/blog",
      path: "/",
      name: "blog",
      component: Blog,
      meta: {
        auth: true,
        layout: layouts.navLeft,
        searchable: true,
        tags: ["Blog"],
        navPath: "/blog"
      }
    },
    {
      path: "/blog/:id",
      name: "blog_detail",
      component: BlogDetail,
      meta: {
        auth: true,
        layout: layouts.navLeft,
        searchable: true,
        tags: ["Detail"],
        navPath: "/blog"
      }
    },
    {
      path: "/blog/create",
      name: "blog_create",
      component: BlogCreate,
      meta: {
        auth: true,
        layout: layouts.navLeft,
        searchable: true,
        tags: ["Detail"],
        navPath: "/blog"
      }
    },
    {
      path: "/blog/edit/:id",
      name: "blog_edit",
      component: BlogEdit,
      meta: {
        auth: true,
        layout: layouts.navLeft,
        searchable: true,
        tags: ["Detail"],
        navPath: "/blog"
      }
    },
    {
      path: "/categories",
      name: "categories",
      component: Category,
      meta: {
        auth: true,
        layout: layouts.navLeft,
        searchable: true,
        tags: ["Categories"],
        navPath: "/categories"
      }
    },

    {
      path: "/jobs",
      name: "jobs",
      component: Job,
      meta: {
        auth: true,
        layout: layouts.navLeft,
        searchable: true,
        tags: ["Jobs"],
        navPath: "/jobs"
      }
    },
    {
      path: "/jobs/:id",
      name: "jobs_detail",
      component: JobDetail,
      meta: {
        auth: true,
        layout: layouts.navLeft,
        searchable: true,
        tags: ["Detail"],
        navPath: "/jobs"
      }
    },
    // {
    {
      path: "/jobs/:id/edit",
      name: "jobs_edit",
      component: JobEdit,
      meta: {
        auth: true,
        layout: layouts.navLeft,
        searchable: true,
        tags: ["Detail"],
        navPath: "/jobs"
      }
    },
    // {
    // 	path: '/chat',
    // 	name: 'chat',
    // 	component: Chat,
    // 	meta: {
    // 		auth: true,
    // 		layout: layouts.navLeft,
    // 		searchable: true,
    // 		tags: ['app'],
    //         navPath: '/chat'
    // 	}
    // },

    {
      path: "/login",
      name: "login",
      component: Login,
      beforeEnter(to, from, next) {
        l.contenOnly();
        next();
      },
      meta: {
        tags: ["Detail"],
        layout: layouts.contenOnly
      }
    },

    {
      path: "/logout",
      name: "logout",
      beforeEnter(to, from, next) {
        auth.logout();
        next({ path: "/login" });
      }
    },
    {
      path: "/accounts",
      name: "account",
      component: Account,
      meta: {
        auth: true,
        layout: layouts.navLeft,
        searchable: true,
        tags: ["Detail"],
        navPath: "/accounts"
      }
    },
    {
      path: "/accounts/:id",
      name: "account_detail",
      component: AccountDetail,
      meta: {
        auth: true,
        layout: layouts.navLeft,
        searchable: true,
        tags: ["Detail"],
        navPath: "/accounts"
      }
    },
    {
      path: "/accounts/:id/edit",
      name: "account_edit",
      component: AccountEdit,
      meta: {
        auth: true,
        layout: layouts.navLeft,
        searchable: true,
        tags: ["Detail"],
        navPath: "/accounts"
      }
    },

    // {
    //     path: "/",
    //     alias: "/dashboard",
    //     name: "dashboard",
    //     component: Dashboard,
    //     meta: {
    //         auth: true,
    //         layout: layouts.navLeft,
    //         searchable: true,
    //         tags: ["Detail"],
    //         navPath: "/dashboard"
    //     }
    // },
    // {
    //     path: '/profile',
    //     name: 'profile',
    //     component: Profile,
    //     meta: {
    //         auth: true,
    //         layout: layouts.navLeft,
    //         searchable: true,
    //         tags: ['pages']
    //     }
    // },
    {
      path: "*",
      name: "not-found",
      component: NotFound,
      meta: {
        layout: layouts.contenOnly
      }
    }
  ]
});

const l = {
  contenOnly() {
    store.commit("setLayout", layouts.contenOnly);
  },
  navLeft() {
    store.commit("setLayout", layouts.navLeft);
  },
  navRight() {
    store.commit("setLayout", layouts.navRight);
  },
  navTop() {
    store.commit("setLayout", layouts.navTop);
  },
  navBottom() {
    store.commit("setLayout", layouts.navBottom);
  },
  set(layout) {
    store.commit("setLayout", layout);
  }
};

//insert here login logic
const auth = {
  loggedIn() {
    return store.getters.isLogged;
  },
  logout() {
    authAPI
      .logout()
      .then(response => {
        this.$store.commit("setOnClickLogout", true);
        this.$store.commit("setAuth", {
          isAuth: false,
          authToken: ""
        });
      })
      .catch(error => {});
  }
};

const searchCofig = {
  setSearchAll(value) {
    store.commit("setSearchAll", value);
  },
  initSearchValue(value) {
    store.commit("setSearchValue", value);
  },
  setIsSearch(value) {
    store.commit("setIsSearch", value);
  }
};

router.beforeEach((to, from, next) => {
  searchCofig.initSearchValue("");

  if (to && to.meta && to.meta.tags) {
    if (to.meta.tags[0] === "Detail") {
      searchCofig.setIsSearch(false);
    } else {
      searchCofig.setIsSearch(true);
    }
  }

  if (
    to.name != "logout" &&
    to.name != "login" &&
    to.meta.tags &&
    to.meta.tags[0] != "Detail"
  ) {
    const {
      meta: { tags }
    } = to;
    store.commit("setSearchTag", tags[0]);
  }
  if (to.name == "dashboard") {
    searchCofig.setSearchAll(true);
  } else {
    searchCofig.setSearchAll(false);
  }

  let authrequired = false;
  if (to && to.meta && to.meta.auth) {
    authrequired = true;
  }

  if (authrequired) {
    console.log("authrequired :", authrequired);
    console.log("store.state.auth.isAuth :", store.state.auth.isAuth);
    if (!store.state.auth.isAuth) {
      next({
        name: "login",
        query: { redirect: to.fullPath }
      });
    } else {
      next();
    }
  } else {
    next();
  }
  /*Check if authentication to set Layout*/
  if (to && to.meta && to.meta.layout && store.state.auth.isAuth) {
    l.set(to.meta.layout);
  }
});
//     let authrequired = true;
//     // if (to && to.meta && to.meta.auth) {
//     //     authrequired = true;
//     // }

//     if (authrequired) {
//         next();
//     } else {
//     }
//     /*Check if authentication to set Layout*/
//     l.set(to.meta.layout);
//     // if (to && to.meta && to.meta.layout && store.state.auth.isAuth) {
//     //     l.set(to.meta.layout);
//     // }
// });

router.afterEach((to, from) => {
  setTimeout(() => {
    store.commit("setSplashScreen", false);
  }, 500);
});

export default router;
