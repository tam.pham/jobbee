var helpers = {
    checkLogin() {
        if (localStorage.isLogged == "true") {
            return true;
        } else {
            return false;
        }
    },
    // getLocalToken() {
    //     return localStorage.jobeeToken;
    // },
    // setLocalProfile(userID, name, userName, avatar) {
    //     localStorage.setItem("userID", userID);
    //     localStorage.setItem("name", name);
    //     localStorage.setItem("userName", userName);
    //     localStorage.setItem("avatar", avatar);
    // },
    // getLocalProfile() {
    //     return {
    //         userID: localStorage.userID,
    //         name: localStorage.name,
    //         userName: localStorage.userName,
    //         avatar: localStorage.avatar
    //     };
    // },
    setLocalToken(token) {
        localStorage.setItem("jobeeToken", token);
        localStorage.setItem("isLogged", true);
    },
    removeLocalToken() {
        localStorage.removeItem("jobeeToken");
        localStorage.setItem("isLogged", false);
    },
    formatPage(page) {
        if (parseInt(page)) return parseInt(page);
        return 1;
    },
    replaceUnderscore(string) {
        if (!string) return "";
        let str = string.replace(/_/g, " ").trim();
        return str.charAt(0).toUpperCase() + str.slice(1);
    },
    formatNumber(number, decimal) {
        // let val = (number / 1).toFixed(decimal).replace(".", ",");
        // return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        decimal == undefined ? (decimal = 2) : null;
        let val = (number / 1).toFixed(decimal);
        return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    },
    /**
     * Get location host and port (ex: localhost:8080)
     * @returns {string}
     */
    getCurrentHost() {
        return (
            window.location.hostname +
            (window.location.port ? ":" + window.location.port : "")
        );
    },

    /**
     * Get host name + port from url (ex: localhost:8080)
     * @param url
     * @returns {string}
     */
    getHostFormUrl(url) {
        var parser = document.createElement("a");

        // Let the browser do the work
        parser.href = url;
        return parser.hostname + (parser.port ? ":" + parser.port : "");
    },

    /**
     * Redirect page to url by using tag a
     * @param url
     */
    redirectTo(url) {
        var a = window.document.createElement("a");
        if (!a.click) {
            //for IE
            window.location = url;
            return;
        }
        a.setAttribute("href", url);
        a.style.display = "none";
        window.document.body.appendChild(a);
        a.click();
    },

    /**
     * check string is interger
     * @param str
     * @returns {boolean}
     */
    isNormalInteger(str) {
        return /^\+?\d+$/.test(str);
    },

    /**
     * validate an integer (bigger than 0)
     * @param rule
     * @param value
     * @param callback
     */
    validateNumber(rule, value, callback) {
        if (value && value.length && !helpers.isNormalInteger(value)) {
            callback(new Error("Please enter number"));
        } else {
            callback();
        }
    },
    urlQueryString(data, allowNULL = true) {
        const url = Object.keys(data);
        return url
            .map(key => {
                if (!allowNULL && data[key] != null) {
                    return [key, data[key]].map(encodeURIComponent).join("=");
                }
            })
            .filter(val => val)
            .join("&");
    }
};

export default helpers;
