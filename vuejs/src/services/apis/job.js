import Axios, { get, post, put } from "axios";
import conf from "../config";
const prefix = "jobs";

const mainUrl = conf.baseUrl + "/" + conf.apiVersion + "/" + prefix;
export default {
    getJobs(
        page = 1,
        limit = 10,
        sort = "id",
        dir = conf.defaultDir,
        status = "",
        workplace_id = "",
        has_activated_post = "",
        from_date = "",
        to_date = "",
        q = "",
        category_ids = []

    ) {
        limit = limit ? limit : 10;
        sort = sort ? sort : "id";
        dir = dir ? dir : conf.defaultDir;
        status = status ? status : "";
        workplace_id = workplace_id ? workplace_id : "";
        has_activated_post = has_activated_post ? has_activated_post : "";
        from_date = from_date ? from_date : "";
        to_date = to_date ? to_date : "";
        category_ids = category_ids ? category_ids : [];
        // if (category_ids) {
            if (category_ids.length >= 1) {
                var temp = 'category_ids[]';
                var temurl = '';
                for (let i = 0; i < category_ids.length; i++) {
                    temurl = temp + '=' + category_ids[i] + '&' + temurl;
                }
                if (from_date && to_date) {
                    let url = `${mainUrl}?page=${page}&limit=${limit}&sort=${sort}&dir=${dir}&${temurl}status=${status}&workplace_id=${workplace_id}&has_activated_post=${has_activated_post}&from_date=${from_date}&to_date=${to_date}&q=${q}`;
                    return get(url);
                } else {
                    let url = `${mainUrl}?page=${page}&limit=${limit}&sort=${sort}&dir=${dir}&${temurl}status=${status}&workplace_id=${workplace_id}&has_activated_post=${has_activated_post}&q=${q}`;
                    return get(url);
                }
            }
            else if(category_ids.length == 0 || !category_ids){
            if (from_date && to_date) {
                let url = `${mainUrl}?page=${page}&limit=${limit}&sort=${sort}&dir=${dir}&status=${status}&workplace_id=${workplace_id}&has_activated_post=${has_activated_post}&from_date=${from_date}&to_date=${to_date}&q=${q}`;
                return get(url);
            } else {
                let url = `${mainUrl}?page=${page}&limit=${limit}&sort=${sort}&dir=${dir}&status=${status}&workplace_id=${workplace_id}&has_activated_post=${has_activated_post}&q=${q}`;
                return get(url);
            }
        }
        let url = `${mainUrl}?page=${page}&limit=${limit}&sort=${sort}&dir=${dir}&status=${status}&workplace_id=${workplace_id}&has_activated_post=${has_activated_post}&q=${q}`;
        return get(url);
    },
    getJob(id) {
        var url = mainUrl + "/" + id;
        return get(url);
    },
    updateJob(id, data) {
        var url = mainUrl + "/" + id;
        return put(url, data);
    },
    getApplications(
        page = 1,
        limit = 10,
        sort = "id",
        dir = conf.defaultDir,
        seeker_id = null,
        job_post_id = null
    ) {
        limit = limit ? limit : 10;
        sort = sort ? sort : "id";
        dir = dir ? dir : conf.defaultDir;
        status = status ? status : "";
        seeker_id = seeker_id ? seeker_id : "";
        job_post_id = job_post_id ? job_post_id : "";

        var url =
            mainUrl +
            "/posts/applications" +
            "?page=" +
            page +
            "&limit=" +
            limit +
            "&sort=" +
            sort +
            "&dir=" +
            dir +
            "&status=" +
            status +
            "&seeker_id=" +
            seeker_id +
            "&job_post_id=" +
            job_post_id;


        return get(url);
    },
    getHirings(
        page = 1,
        limit = 10,
        sort = "id",
        dir = conf.defaultDir,
        job_post_id = null
    ) {
        limit = limit ? limit : 10;
        sort = sort ? sort : "id";
        dir = dir ? dir : conf.defaultDir;
        job_post_id = job_post_id ? job_post_id : "";

        var url =
            mainUrl +
            "/posts/hirings" +
            "?page=" +
            page +
            "&limit=" +
            limit +
            "&sort=" +
            sort +
            "&dir=" +
            dir +
            "&job_post_id=" +
            job_post_id;

        return get(url);
    },
    getApplicationsByPost(
        page = 1,
        limit = 10,
        sort = "id",
        dir = conf.defaultDir,
        postID
    ) {
        return this.getApplications(page, limit, sort, dir, null, postID);
    },
    getHiringsByPost(
        page = 1,
        limit = 10,
        sort = "id",
        dir = conf.defaultDir,
        postID
    ) {
        return this.getHirings(page, limit, sort, dir, postID);
    },

    getApplicationsBySeeker(
        seekerID,
        page = 1,
        limit = 10,
        sort = "id",
        dir = conf.defaultDir
    ) {
        return this.getApplications(page, limit, sort, dir, seekerID);
    },
    /**
     *
     * Create new job image
     * @param {*} data
     * @returns
     */
    createJobImage(data) {
        let url = mainUrl + "/images";
        return post(url, data);
    },
    /**
     * Get all images job
     * @param {*} id
     * @return {*} object
     */
    getImagesJob(id) {
        let url = mainUrl + "/images?job_id=" + id;
        return get(url);
    },

    /**
     *
     *
     * @param {number} id of job images
     * @param {*} data
     * @returns {string} url
     */
    updatePostionImages(id, data) {
        let url = mainUrl + "/images/" + id;
        console.log("url of update image :", url);
        return put(url, data);
    },
    /**
     *
     *
     * @param {*} id
     * @returns
     */
    removeJobImages(id) {
        let url = mainUrl + "/images/" + id;
        console.log("url of update image :", url);
        return Axios.delete(url);
    }
};
