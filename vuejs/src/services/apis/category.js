import { get, post, put } from "axios";
import axios from "axios";
import conf from "../config";
const prefix = "jobCategory";

const mainUrl = conf.baseUrl + prefix;
export default {
  getCategories() {
    return get(mainUrl);
  },
  getCategory(id) {
    var url = mainUrl + "/" + id;
    return get(url);
  },
  addCategory(data) {
    var url = mainUrl;
    return post(url, data);
  },
  updateCategory(data, icon = "", updateIcon = false) {
    var url = mainUrl + "/" + data.id;
    if (updateIcon) data.icon_path = icon;
    return put(url, data);
  },
  removeCategory(id) {
    var url = mainUrl + "/" + id;
    return axios.delete(url);
  }
};
