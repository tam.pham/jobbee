import Axios, { get, post, put } from "axios";
import conf from "../config";
const prefix = "blog";

const mainUrl = conf.baseUrl + prefix;
export default {
    getBlogs() {
        return get(mainUrl);
    },
    getBlog(id) {
        var url = mainUrl + "/" + id;
        return get(url);
    },
    createBlog(data) {
        let url = mainUrl;
        return post(url, data);
    },
    updateBlog(data, id) {
        let url = mainUrl + "/" + id;
        return put(url, data);
    },
    deleteBlog(id) {
        return Axios.delete(mainUrl + "/" + id);
    }
};
