import { get, post, put } from "axios";
import conf from "../config";
const prefix = "documents";

const mainUrl = conf.baseUrl + "/" + conf.apiVersion + "/" + prefix;

const termsAndConditionsSlug = "terms-and-conditions";
// const termsAndConditionsSlug = '2';
export default {
    getDocuments(page = 1, limit = 10, sort = "id", dir = conf.defaultDir) {
        page = page ? page : 1;
        limit = limit ? limit : 10;
        sort = sort ? sort : "id";
        dir = dir ? dir : conf.defaultDir;

        var url =
            mainUrl +
            "?page=" +
            page +
            "&limit=" +
            limit +
            "&sort=" +
            sort +
            "&dir=" +
            dir;
        return get(url);
    },
    getTermsAndConditions() {
        return this.getDocument(termsAndConditionsSlug);
    },
    saveTermsAndConditions(data) {
        return this.saveDocument(termsAndConditionsSlug, data);
    },
    getDocument(slug) {
        var url = mainUrl + "/" + slug;
        return get(url);
    },
    saveDocument(id, data) {
        var url = mainUrl + "/" + id;
        return put(url, data);
    },
    getPreviewSeekerProfile(data) {
        var url = mainUrl + "/" + "seeker/curriculum-vitae/preview";
        console.log("data :", data);
        console.log("url :", url);
        return post(url, data);
    }
};
