import axios from "axios";

import store from "~/store/index";

import router from "~/router/";

import helper from "../helper";

import auth from "./auth";
import account from "./account";
import job from "./job";
import seeker from "./seeker";
import employer from "./employer";
// import workplace from "./workplace";
import blog from "./blog";
import category from "./category";
import media from "./media";
import document from "./document";
import role from "./role";
import permission from "./permission";
import statistic from "./statistic";

axios.defaults.headers.common = {
    // Authorization: "Bearer " + helper.getLocalToken(),
    "Content-Type": "application/json",
    Accept: "application/json",
    "Cache-Control": "no-cache",
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Credentials": false
};
// axios.interceptors.request.use(
//     config => {
//         config.headers.Authorization = "Bearer " + store.state.auth.authToken;
//         return config;
//     },
//     error => {
//         return Promise.reject(error);
//     }
// );

axios.interceptors.response.use(
    response => {
        //console.log("success..." + response);
        return response;
    },
    error => {
        // console.log("error..." + error);
        if (
            error.response &&
            error.response.status != 200 &&
            window.location.pathname != "/login"
        ) {
            if (router.history.current.name != "login") {
                store.commit("setAuth", {
                    isAuth: false,
                    authToken: ""
                });
                router.replace({ name: "login" });
            }
        }
        return Promise.reject(error);
    }
);

export {
    auth,
    account,
    seeker,
    employer,
    job,
    category,
    blog,
    media,
    document,
    role,
    permission,
    statistic
};
