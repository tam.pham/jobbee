import Axios, { get } from "axios";
import conf from "../config";
const prefix = "statistics";
const mainUrl = conf.baseUrl + "/" + conf.apiVersion + "/" + prefix;
const seekers = "seekers";
const workplaces = "workplaces";
const chats = "chats";
const jobs_post = "jobs/posts";
const jobs_offer = "jobs/posts/offers";
const honeys = "honeys";
const jobs_application = "jobs/posts/applications";
const employers = "employers";

export default {
    getTrafficSeeker(start_date, from_date, type) {
        let url =
            mainUrl +
            `/${seekers}/traffic?start_date=${start_date}&end_date=
        ${from_date}&type=${type}`;
        return get(url);
    },
    getTrafficWorkplace(start_date, from_date) {
        let url =
            mainUrl +
            `/${workplaces}/traffic?start_date=${start_date}&end_date=
        ${from_date}`;
        return get(url);
    },
    getTrafficChat(start_date, from_date) {
        let url =
            mainUrl +
            `/${chats}/traffic?start_date=${start_date}&end_date=
        ${from_date}`;
        return get(url);
    },
    getTrafficJobPost(start_date, from_date) {
        let url =
            mainUrl +
            `/${jobs_post}/traffic?start_date=${start_date}&end_date=
        ${from_date}`;
        return get(url);
    },
    getTrafficJobOffer(start_date, from_date) {
        let url =
            mainUrl +
            `/${jobs_offer}/traffic?start_date=${start_date}&end_date=
        ${from_date}`;
        return get(url);
    },
    getTrafficHoneySpending(start_date, from_date, type = "spending") {
        let url =
            mainUrl +
            `/${honeys}/traffic?start_date=${start_date}&end_date=
        ${from_date}&type=${type}`;
        return get(url);
    },
    getTrafficHoneyBuying(start_date, from_date, type = "buying") {
        let url =
            mainUrl +
            `/${honeys}/traffic?start_date=${start_date}&end_date=
        ${from_date}&type=${type}`;
        return get(url);
    },
    getJobApplicationTraffic(start_date, from_date) {
        let url =
            mainUrl +
            `/${jobs_application}/traffic?start_date=${start_date}&end_date=
        ${from_date}`;
        return get(url);
    },
    getEmployerTraffic(start_date, from_date, type) {
        let url =
            mainUrl +
            `/${employers}/traffic?start_date=${start_date}&end_date=
    ${from_date}&type=${type}`;
        return get(url);
    }
};
