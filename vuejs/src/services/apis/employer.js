import { get, post, put } from "axios";
import conf from "../config";
import $ from "jquery";
import helper from "../helper";
import store from "~/store/index";

const prefix = "user/all2";

const mainUrl = conf.baseUrl;
export default {
  getEmployers() {
    return get(mainUrl + prefix);
  },

  getEmployer(id) {
    var url = mainUrl + "user/" + id;
    return get(url);
  },
  updateStatus(id, data) {
    var url = conf.baseUrl + "user/update/" + id;
    return put(url, data);
  }
};
