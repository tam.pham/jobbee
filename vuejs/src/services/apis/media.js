import { get, post } from "axios";
import conf from "../config";
const prefix = "blog/upload";
const mainUrl = conf.baseUrl + prefix;

export default {
  upload(file) {
    var data = new FormData();
    data.append("file", file);
    return post(mainUrl, data);
  }
};
