import  { get, post, put} from 'axios';
import axios from "axios";
import conf from '../config'
const prefix = 'permissions';

const mainUrl = conf.baseUrl + '/' + conf.apiVersion + '/' + prefix;
export default {
    getPermissions(){
        return get(mainUrl);
    },
    addPermission(data){
        var url = mainUrl;
        return post(url, data);
    },
    updatePermission(id, data){
        var url = mainUrl + "/" + id;
        return put(url, data);
    },
    removePermission(id){
        var url = mainUrl + "/" + id;
        return axios.delete(url);
    },

}
