import axios, { get, post, put } from 'axios';
import conf from '../config'
const prefix = 'account';
const path = {
};
const mainUrl = conf.baseUrl + '/' + conf.apiVersion + '/' + prefix;
export default {
    // getAccounts(page = 1, limit = 10, sort = 'id', dir, is_verified = 'all', q = '' ){
    //     // console.log(page, limit, sort, dir);
    //     page = page ? page : 1;
    //     limit = limit ? limit : 10;
    //     sort =  sort ? sort : 'id';
    //     dir =  dir ? dir : conf.defaultDir;
    //     let verify = (is_verified != 'all' && is_verified != null) ? ('&is_verified=' + is_verified) : "";

    //     var url = mainUrl
    //         + '?page=' + page
    //         + '&limit=' + limit
    //         + '&sort=' + sort
    //         + '&dir=' + dir
    //         + verify
    //         + '&q=' + q;
    //     return get(url);
    // },
    getAccounts(page = 1, limit = 10, sort = 'id', dir) {
              page = page ? page : 1;
        limit = limit ? limit : 10;
        sort =  sort ? sort : 'id';
        dir =  dir ? dir : conf.defaultDir;
             var url = mainUrl
            + '?page=' + page
            + '&limit=' + limit
            + '&sort=' + sort
            + '&dir=' + dir
        return get(url);
    },
    getAccount(id){
        var url = mainUrl + '/' + id
        return get(url);
    },
    removeAccount(id){
        var url = mainUrl + "/" + id;
        return axios.delete(url);
    },
    updateAccount(id,data) {
        var url = mainUrl + '/' + id
        return put(url, data);
    },
    createAccount(data) {
        var url = mainUrl;
        return post(url, data);
    }
}