import  Axios,{ get, post, put } from 'axios';
import conf from '../config'
const prefix = 'workplaces';

const mainUrl = conf.baseUrl + '/' + conf.apiVersion + '/' + prefix;
export default {
    getWorkplaces(page = 1, limit = 10, sort = 'id',dir = conf.defaultDir, employer_id = "", is_verified = 'all', q = '' ){

        limit = limit ? limit : 10;
        sort =  sort ? sort : 'id';
        dir =  dir ? dir : conf.defaultDir;

        let verify = (is_verified != 'all' && is_verified != null) ? ('&is_verified=' + is_verified) : "";
        var url = mainUrl
            + '?page=' + page
            + '&limit=' + limit
            + '&sort=' + sort
            + '&dir=' + dir
            + '&employer_id=' + employer_id
            + verify
            + '&q=' + q;
        return get(url);
    },
    getWorkplace(id){
        var url = mainUrl + '/' + id
        return get(url);
    },
    updateWorkplace(id, data){
        var url = mainUrl + '/' + id
        return put(url, data);
    },
    verify(id, value) {
        let url;
        if (value) {
            url = mainUrl + "/" + id + "/verify";
        } else {
            url = mainUrl + "/" + id + "/unverified";
        }
        return post(url);
    },

    /**
     *
     * Create new company image
     * @param {*} data
     * @returns
     */
    createCompanyImage(data) {
        let url = mainUrl + '/images' ;
        return post(url,data)
    },
    /**
     * Get all images company
     * @param {*} id
     * @return {*} object
     */
    getImagesCompany(id) {
        let url = mainUrl + '/images?workplace_id=' + id
        return get(url)
    },

    /**
     *
     *
     * @param {number} id of company images
     * @param {*} data
     * @returns {string} url
     */
    updatePostionImages(id, data) {
        let url = mainUrl + '/images/' + id
        console.log('url of update image :', url);
        return put(url,data)
    },

    /**
     *
     *
     * @param {*} id
     * @returns
     */
    removeCompanyImages(id) {
        let url = mainUrl + '/images/' + id
        console.log('url of update image :', url);
        return Axios.delete(url)
    }


}
