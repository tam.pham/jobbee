import Axios, { get, post, put } from "axios";
import conf from "../config";
import helper from "../helper";
const prefix = "user/all1";

const mainUrl = conf.baseUrl + prefix;
export default {
  getSeekers() {
    return get(mainUrl);
  },

  getSeeker(id) {
    var url = mainUrl + "/" + id;
    return get(url);
  },
  updateStatus(id, data) {
    var url = conf.baseUrl + "user/update/" + id;
    return put(url, data);
  }
};
