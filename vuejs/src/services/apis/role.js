import  { get, post, put} from 'axios';
import axios from "axios";
import conf from '../config'
const prefix = 'roles';

const mainUrl = conf.baseUrl + '/' + conf.apiVersion + '/' + prefix;
export default {
    getRoles(page = 1, limit = 10, sort = 'id',dir = conf.defaultDir, q = '' ){
        page = page ? page : 1;
        limit = limit ? limit : 10;
        sort =  sort ? sort : 'id';
        dir =  dir ? dir : conf.defaultDir;

        var url = mainUrl
            + '?page=' + page
            + '&limit=' + limit
            + '&sort=' + sort
            + '&dir=' + dir
            + '&q=' + q;
        return get(url);
    },
    getRole(id){
        var url = mainUrl + '/' + id
        return get(url);
    },
    addRole(data){
        var url = mainUrl;
        return post(url, data);
    },
    updateRole(id, data){
        var url = mainUrl + "/" + id;
        return put(url, data);
    },
    removeRole(id){
        var url = mainUrl + "/" + id;
        return axios.delete(url);
    },

}