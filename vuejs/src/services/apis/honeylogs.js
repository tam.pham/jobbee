import { get, post } from "axios";
import conf from "../config";

const prefix = "honeys";

const mainUrl = conf.baseUrl + "/" + conf.apiVersion + "/" + prefix;

export default {
    getHoneyLogs(
        page = "",
        limit = 10,
        sort = "id",
        dir,
        id,
        type,
        from_date,
        to_date
    ) {
        page = page ? page : 1;
        limit = limit ? limit : 10;
        sort = sort ? sort : "id";
        dir = dir ? dir : conf.defaultDir;
        id = id ? id : "";
        type = type ? type : "";
        from_date = from_date ? from_date : "";
        to_date = to_date ? to_date : "";
        if (type) {
            var url =
                mainUrl +
                "/logs" +
                "?page=" +
                page +
                "&limit=" +
                limit +
                "&sort=" +
                sort +
                "&dir=" +
                dir +
                "&user_id=" +
                id +
                "&type=" +
                type +
                "&from_date=" +
                from_date +
                "&to_date=" +
                to_date;
        } else {
            var url =
                mainUrl +
                "/logs" +
                "?page=" +
                page +
                "&limit=" +
                limit +
                "&sort=" +
                sort +
                "&dir=" +
                dir +
                "&user_id=" +
                id +
                "&from_date=" +
                from_date +
                "&to_date=" +
                to_date;
        }
        return get(url);
    }
};
