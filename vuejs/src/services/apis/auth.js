import { get, post } from "axios";
import conf from "../config";
const prefix = "auth";
const path = {
    login: "token",
    logout: "logout",
    user: "me"
};

const mainUrl = "http://localhost:8080";
console.log("mainUrl :", mainUrl);

export default {
    login(email, pass) {
        var url = mainUrl + "/api/login/admin";
        var data = {
            email: email,
            password: pass
        };
        console.log("url :", url);
        console.log("data :", data);
        return post(url, data);
    },
    logout() {
        var url = mainUrl + "/" + path.logout;
        return post(url);
    }
    // getUser() {
    //     var url = mainUrl + "/" + path.user;
    //     return get(url);
    // }
};
