export default {
    baseUrl: process.env.VUE_APP_API_URL || "http://localhost:8080/api/",
    itemPerPage: 10,
    defaultDir: "desc",
    defaultSort: "id",
    commitHash: process.env.VUE_APP_COMMIT_HASH,
    avtHolder: require("@/assets/images/avatar_placeholder.png"),
    imgHolder: require("@/assets/images/image_placeholder.png")
};
