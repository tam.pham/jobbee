/* ============
 * Getter of the all module
 * ============
 *
 * The initial getter of the all module.
 */
export default {
    layout: (state, getters) => state.layout,

    navPos: (state, getters) => state.layout.navPos,

    toolbar: (state, getters) => state.layout.toolbar,

    footer: (state, getters) => state.layout.footer,

    boxed: (state, getters) => state.layout.boxed,

    roundedCorners: (state, getters) => state.layout.roundedCorners,

    viewAnimation: (state, getters) => state.layout.viewAnimation,

    isLogged: (state, getters) => state.auth.isAuth,

    splashScreen: (state, getters) => state.splashScreen,

    getProfile: state => state.profile,

    getSearchValue: state => state.search
};
