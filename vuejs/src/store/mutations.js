/* ============
 * Mutations for the all module
 * ============
 *
 */

export default {
    setLayout: (state, payload) => {
        if (payload && payload.navPos !== undefined)
            state.layout.navPos = payload.navPos;

        if (payload && payload.toolbar !== undefined)
            state.layout.toolbar = payload.toolbar;

        if (payload && payload.footer !== undefined)
            state.layout.footer = payload.footer;

        if (payload && payload.boxed !== undefined)
            state.layout.boxed = payload.boxed;

        if (payload && payload.roundedCorners !== undefined)
            state.layout.roundedCorners = payload.roundedCorners;

        if (payload && payload.viewAnimation !== undefined)
            state.layout.viewAnimation = payload.viewAnimation;
    },
    setLogin: (state, payload) => {
        state.logged = true;
    },
    setLogout: (state, payload) => {
        state.layout.navPos = null;
        state.layout.toolbar = null;
        state.logged = false;
    },
    setSplashScreen: (state, payload) => {
        state.splashScreen = payload;
    },
    setSearchAll: (state, value) => {
        state.searchAll = value;
    },
    setSearchValue: (state, value) => {
        state.search = value;
    },
    setSearchSubmit: (state, value) => {
        state.searchSubmit = value;
    },
    setSearchTag: (state, value) => {
        state.searchTag = value;
    },
    setOnClickLogout: (state, value) => {
        state.isLogoutClick = value;
    },
    setIsSearch: (state, value) => {
        state.isSearch = value;
    },
    initProfile: (state, value) => (state.app.me = value),
    setAuth: (state, value) => (state.auth = value)
};
