/* ============
 * State of the all module
 * ============
 *
 * The initial state of the all module.
 */
import helper from "../services/helper";
import config from "../services/config";

export default {
    layout: {
        navPos: null, //top, bottom, left, right, false
        toolbar: null, //top, bottom, false
        footer: true, //above, below, false
        boxed: false, //true, false
        roundedCorners: false, //true, false
        viewAnimation: "fade-top" // fade-left, fade-right, fade-top, fade-top-in-out, fade-bottom, fade-bottom-in-out, fade, false
    },
    splashScreen: false,
    logged: helper.checkLogin(),
    // profile: helper.getLocalProfile(),
    search: "",
    itemPerPage: config.itemPerPage ? config.itemPerPage : 10,
    isSearch: true,
    searchAll: false,
    searchSubmit: "",
    searchTag: "",
    isLogoutClick: false,
    commitHash: config.commitHash,
    avtHolder: config.avtHolder,
    imgHolder: config.imgHolder,
    app: {
        me: {
            name: "admin"
        }
    },
    auth: {
        isAuth: false,
        authToken: ""
    }
};
