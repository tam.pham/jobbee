import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

import actions from "./actions";
import getters from "./getters";
import mutations from "./mutations";
import state from "./state";

const debug = process.env.NODE_ENV !== "production";
export default new Vuex.Store({
    namespaced: true,
    state,
    getters,
    mutations,
    actions,
    plugins: [createPersistedState({ paths: ["layout", "auth"] })],
    strict: debug
});
