var path = require("path");
const fs = require('fs')
const dotenv = require('dotenv')

var defaultEnv = {};

module.exports = {
    /**
     * get env from files (.env.[mode], .env.[mode].local)
     * @param mode
     */
    loadEnv (mode){
        const basePath = path.resolve(__dirname, `.env${mode ? `.${mode}` : ``}`);
        const localPath = `${basePath}.local`;

        const load = path => {
            try {
                let encoding /*: string */ = 'utf8';
                let debug = false;
                const parsed = dotenv.parse(fs.readFileSync(path, { encoding }), { debug });
                Object.keys(parsed).forEach(function (key) {
                    if (!defaultEnv.hasOwnProperty(key)) {
                        defaultEnv[key] = parsed[key]
                    }
                })
            } catch (err) {

            }
        }

        load(localPath);
        load(basePath);
    },
    /**'
     * check process.env after run `set` command, reset if `set` empty value
     */
    resetENV(){
        if (process.env.NODE_ENV) {
            this.loadEnv(process.env.NODE_ENV)
        }
        this.loadEnv();

        Object.keys(defaultEnv).forEach(function (key) {
            process.env[key] = process.env[key].trim();
            if (!process.env[key]) {
                process.env[key] = defaultEnv[key];
            }
        })
    }
};
