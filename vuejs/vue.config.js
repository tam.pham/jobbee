const webpack = require("webpack");

const { CleanWebpackPlugin } = require("clean-webpack-plugin");

const CompressionPlugin = require("compression-webpack-plugin");

var path = require("path");
var helper = require("./helper.js");

helper.resetENV();

let commitHash = require("child_process")
    .execSync("git rev-parse --short HEAD")
    .toString()
    .trim();

process.env.VUE_APP_COMMIT_HASH = commitHash;

if (process.env.NODE_ENV === "production") {
    module.exports = {
        lintOnSave: process.env.NODE_ENV !== "production",
        configureWebpack: {
            resolve: {
                alias: {
                    "~": path.resolve(__dirname, "src")
                }
            },

            entry: "./src/main.js",
            output: {
                filename: "[hash].bundle.js",
                path: path.resolve(__dirname, "dist")
            },
            plugins: [
                /*
                 *Enable this comment for auto generate Gzip
                 * Do not delete
                 new CompressionPlugin({
                         filename: "[path].gz[query]",
                         algorithm: "gzip",
                         test: /\.js$|\.css$|\.html$/,
                         threshold: 10240,
                         minRatio: 0.8
                     }),
                 */
                new CleanWebpackPlugin({
                    cleanOnceBeforeBuildPatterns: ["dist"]
                }),
                new webpack.HashedModuleIdsPlugin()
            ],
            optimization: {
                flagIncludedChunks: true,
                mergeDuplicateChunks: true,
                removeEmptyChunks: true,
                removeAvailableModules: true,
                namedModules: true,
                namedChunks: true,
                runtimeChunk: true,
                splitChunks: {
                    chunks: "all",
                    maxInitialRequests: Infinity, //in Http/2  Maximum number of rallel requests at an entry point.
                    maxSize: 2000000, // maxSize to splitting
                    cacheGroups: {
                        vendor: {
                            test: /[\\/]node_modules[\\/]/,
                            name: "vendors",
                            chunks: "all",
                            priority: 9,
                            minSize: 200000 // minSize of chunk is 200kb
                        },
                        default: {
                            minChunks: Infinity, // Minimum number of chunks that must share a module before splitting
                            priority: -20,
                            reuseExistingChunk: true
                        }
                    }
                }
            }
        },
        productionSourceMap: false
    };
} else {
    module.exports = {
        lintOnSave: process.env.NODE_ENV !== "production",
        configureWebpack: {
            devtool: "source-map",
            resolve: {
                alias: {
                    "~": path.resolve(__dirname, "src")
                }
            }
        },
        productionSourceMap: false
    };
}
