package com.aptech.jobbee.service;

import java.util.List;

import com.aptech.jobbee.entity.CommentTbl;

public interface CommentService {

	List<CommentTbl> findByBlogId(String blogId);
	
	void save(CommentTbl commentTbl);
	
}