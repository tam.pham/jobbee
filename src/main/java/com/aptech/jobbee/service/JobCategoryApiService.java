package com.aptech.jobbee.service;

import java.util.Optional;

import com.aptech.jobbee.entity.JobCategoryTbl;

public interface JobCategoryApiService {

	JobCategoryTbl createJobCategory(JobCategoryTbl jobCategoryTbl);
	Optional<JobCategoryTbl> findById(Long id);
	void save(JobCategoryTbl jobCategoryTbl);
	void remove(JobCategoryTbl jobCategoryTbl);
	
	Iterable<JobCategoryTbl> findAll();
}