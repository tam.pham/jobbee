package com.aptech.jobbee.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aptech.jobbee.entity.JobCategoryTbl;
import com.aptech.jobbee.respository.JobCategoryApiRepository;
import com.aptech.jobbee.service.JobCategoryApiService;

@Service
public class JobCategoryApiServiceImpl implements JobCategoryApiService{

	@Autowired
	JobCategoryApiRepository jobCategoryApiRepository;

	@Override
	public JobCategoryTbl createJobCategory(JobCategoryTbl jobCategoryTbl) {
		return null;
	}

	@Override
	public Optional<JobCategoryTbl> findById(Long id) {
		return jobCategoryApiRepository.findById(id);
	}

	@Override
	public void save(JobCategoryTbl jobCategoryTbl) {
		jobCategoryApiRepository.save(jobCategoryTbl);
		
	}

	@Override
	public void remove(JobCategoryTbl jobCategoryTbl) {
		jobCategoryApiRepository.delete(jobCategoryTbl);
	}

	@Override
	public Iterable<JobCategoryTbl> findAll() {
		return jobCategoryApiRepository.findAll();
	}
}
