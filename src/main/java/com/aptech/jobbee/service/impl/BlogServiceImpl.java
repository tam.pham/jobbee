package com.aptech.jobbee.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aptech.jobbee.entity.BlogComment;
import com.aptech.jobbee.entity.BlogTbl;
import com.aptech.jobbee.respository.BlogRespository;
import com.aptech.jobbee.service.BlogService;

@Service
public class BlogServiceImpl implements BlogService{

	@Autowired
	BlogRespository blogRespository;

	@Override
	public BlogTbl createContact(BlogTbl entity) {
		return null;
	}

	@Override
	public BlogTbl findById(int id) {
		return blogRespository.findById(id);
	}

	@Override
	public List<BlogTbl> findAll() {
		return blogRespository.findAll();
	}

	@Override
	public void save(BlogTbl blogTbl) {
		blogRespository.save(blogTbl);
	}

	@Override
	public void remove(BlogTbl blogTbl) {
		blogRespository.delete(blogTbl);
	}

	@Override
	public List<BlogTbl> getActiveBlog() {
		return blogRespository.getActiveBlog();
	}

	@Override
	public List<BlogComment> getTopComment() {
		return blogRespository.getTopComment();
	}

}