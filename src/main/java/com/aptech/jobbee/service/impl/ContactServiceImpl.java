package com.aptech.jobbee.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aptech.jobbee.entity.ContactTbl;
import com.aptech.jobbee.respository.ContactRespository;
import com.aptech.jobbee.service.ContactService;

@Service
public class ContactServiceImpl implements ContactService{

	@Autowired
	ContactRespository contactRespository;
	
	@Override
	public ContactTbl createContact(ContactTbl entity) {
		return contactRespository.save(entity);
	}

	@Override
	public ContactTbl updateContact(ContactTbl entity) {
		return contactRespository.save(entity);
	}

	@Override
	public ContactTbl findById(Long id) {
		return contactRespository.findById(id).get();
	}

	@Override
	public void deleteContact(ContactTbl entity) {
		// TODO Auto-generated method stub
		
	}

}
