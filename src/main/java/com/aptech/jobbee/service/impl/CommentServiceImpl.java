package com.aptech.jobbee.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aptech.jobbee.entity.CommentTbl;
import com.aptech.jobbee.respository.CommentRespository;
import com.aptech.jobbee.service.CommentService;

@Service
public class CommentServiceImpl implements CommentService{

	@Autowired
	CommentRespository commentRespository;

	@Override
	public List<CommentTbl> findByBlogId(String blogId) {
		return commentRespository.getListCommentByBlogId(blogId);
	}

	@Override
	public void save(CommentTbl commentTbl) {
		commentRespository.save(commentTbl);
	}


}