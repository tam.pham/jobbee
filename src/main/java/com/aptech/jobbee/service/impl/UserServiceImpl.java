package com.aptech.jobbee.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aptech.jobbee.entity.UserTbl;
import com.aptech.jobbee.respository.UserRespository;
import com.aptech.jobbee.service.UserService;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	UserRespository userRespository;
	
	
	@Override
	@Transactional
	public UserTbl createUser(UserTbl user) {
		return userRespository.save(user);
	}

	@Override
	public UserTbl getUserById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserTbl ckeckLogin(String userName, String password) {
		return userRespository.checkLogin(userName, password);
	}

    @Override
    public boolean isExistEmail(String email) {
        return userRespository.isExitsEmail(email);
    }

	@Override
	public List<UserTbl> getAllUser() {
		return userRespository.getAllUser();
	}

	@Override
	public void saveUser(UserTbl user) {
		userRespository.save(user);
	}

	@Override
	public UserTbl findById(int id) {
		return userRespository.findById(id);
	}

	@Override
	public List<UserTbl> getAllUserByType(int userType) {
		return userRespository.getAllUserByType(userType);
	}

}
