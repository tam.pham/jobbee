package com.aptech.jobbee.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aptech.jobbee.entity.CompanyTbl;
import com.aptech.jobbee.entity.JobPostTbl;
import com.aptech.jobbee.respository.CompanyRespository;

@Service
public class CompanyService {
	@Autowired
	CompanyRespository companyRespository;
	
	public List<CompanyTbl> listCompany(){
		return (List<CompanyTbl>) companyRespository.findAll();
	}
	
	public void deleteCompany(String id) {
		companyRespository.deleteCompany(id);
	}

	public void createCompany(CompanyTbl companyTbl) {
		companyRespository.createCompany(companyTbl);
	}
	
    public CompanyTbl getByCompanyName(String comName) {
        return companyRespository.getByCompanyName(comName);
    }
    
    public void updateCompany(CompanyTbl companyTbl) {
        companyRespository.updateCompany(companyTbl);
    }
    
    public CompanyTbl getCompanyById(String id){
	    return companyRespository.getCompanyById(id);
    }
    
    public List<CompanyTbl> getAllCompany() {
		return companyRespository.findAll();
	}
}
