package com.aptech.jobbee.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aptech.jobbee.entity.JobCategoryTbl;
import com.aptech.jobbee.respository.JobCategoryRespository;

@Service
public class JobCategoryService {
	@Autowired
	JobCategoryRespository jobCategoryRespository;
	
	public List<JobCategoryTbl> listJobCategory(){
		return (List<JobCategoryTbl>) jobCategoryRespository.findAll();
	}
}
