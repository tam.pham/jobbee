package com.aptech.jobbee.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aptech.jobbee.entity.JobTypeTbl;
import com.aptech.jobbee.entity.LocationTbl;
import com.aptech.jobbee.respository.JobTypeRespository;
import com.aptech.jobbee.respository.LocationRepository;

@Service
public class JobTypeService {
	@Autowired
	JobTypeRespository jobTypeRespository;
	
	public List<JobTypeTbl> listJobType(){
		return (List<JobTypeTbl>) jobTypeRespository.findAll();
	}
}
