package com.aptech.jobbee.service;

import java.util.List;

import com.aptech.jobbee.entity.BlogComment;
import com.aptech.jobbee.entity.BlogTbl;

public interface BlogService {

	BlogTbl createContact(BlogTbl blogTbl);
	BlogTbl findById(int id);
	void save(BlogTbl blogTbl);
	void remove(BlogTbl blogTbl);
	
	List<BlogTbl> findAll();
	
	List<BlogTbl> getActiveBlog();
	
	List<BlogComment> getTopComment();
	
}