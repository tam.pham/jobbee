package com.aptech.jobbee.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aptech.jobbee.entity.JobPostTbl;
import com.aptech.jobbee.respository.JobPostRespository;

@Service
public class JobPostService {
	
	@Autowired
	JobPostRespository jobPostRespository;
	
	public void createJobPost(JobPostTbl jobTbl) {
		jobPostRespository.createJob(jobTbl);
	}
	public void deleteJob(String id) {
		jobPostRespository.deleteJob(id);
	}

    public void updateJob(JobPostTbl jobTbl) {
        jobPostRespository.updateJob(jobTbl);
    }
	
	public List<JobPostTbl> getAllJob() {
		return jobPostRespository.getAllJob();
	}

	public JobPostTbl getJobById(String id){
	    return jobPostRespository.getJobById(id);
    }
	
	public List<JobPostTbl> findJobPost(String jobTitle, String categoryName) {
		return jobPostRespository.findJobPost(jobTitle, categoryName);
	}
}
