package com.aptech.jobbee.service;

import com.aptech.jobbee.entity.ContactTbl;

public interface ContactService {
	ContactTbl createContact(ContactTbl entity);
	ContactTbl updateContact(ContactTbl entity);
	ContactTbl findById(Long id);
	void deleteContact(ContactTbl entity);
}
