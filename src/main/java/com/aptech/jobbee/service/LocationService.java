package com.aptech.jobbee.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aptech.jobbee.entity.LocationTbl;
import com.aptech.jobbee.respository.LocationRepository;

@Service
public class LocationService {
	@Autowired
	LocationRepository locationRepository;
	
	public List<LocationTbl> listLocation(){
		return (List<LocationTbl>) locationRepository.findAll();
	}
}
