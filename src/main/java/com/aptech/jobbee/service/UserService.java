package com.aptech.jobbee.service;

import java.util.List;

import com.aptech.jobbee.entity.UserTbl;

public interface UserService {
	UserTbl createUser(UserTbl user);
	UserTbl getUserById(int id);
	UserTbl ckeckLogin(String userName, String password);
	boolean isExistEmail(String email);
	
	void saveUser(UserTbl user);
	
	UserTbl findById(int id);
	
	List<UserTbl> getAllUser();
	
	List<UserTbl> getAllUserByType(int userType);
	
}
