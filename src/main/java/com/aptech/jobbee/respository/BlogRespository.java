package com.aptech.jobbee.respository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.aptech.jobbee.entity.BlogComment;
import com.aptech.jobbee.entity.BlogTbl;


@Repository
public interface BlogRespository extends CrudRepository<BlogTbl, Integer>{
	
	List<BlogTbl> getActiveBlog();
	
	BlogTbl findById(int id);
	
	List<BlogComment> getTopComment();
	
	List<BlogTbl> findAll();
	
}
