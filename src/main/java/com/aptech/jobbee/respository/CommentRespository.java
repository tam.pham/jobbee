package com.aptech.jobbee.respository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.aptech.jobbee.entity.CommentTbl;


@Repository
public interface CommentRespository extends CrudRepository<CommentTbl, Integer>{
	
	List<CommentTbl> getListCommentByBlogId(String blogId);

}
