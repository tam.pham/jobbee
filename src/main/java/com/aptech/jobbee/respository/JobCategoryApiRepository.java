package com.aptech.jobbee.respository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.aptech.jobbee.entity.JobCategoryTbl;

@Repository
public interface JobCategoryApiRepository extends CrudRepository<JobCategoryTbl, Long>{
	
	

}
