package com.aptech.jobbee.respository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.aptech.jobbee.entity.UserTbl;

@Repository
public interface UserRespository extends CrudRepository<UserTbl, Integer>{
	UserTbl checkLogin(String userName, String password);
	boolean isExitsEmail(String email);
	
	List<UserTbl> getAllUser();
	
	List<UserTbl> getAllUserByType(int userType);
	UserTbl findById(int id);
}
