package com.aptech.jobbee.respository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.aptech.jobbee.entity.CommentTbl;
import com.aptech.jobbee.respository.CommentRespository;


@Repository
public abstract class CommentRespositoryImpl implements CommentRespository{
	
	@PersistenceContext
    EntityManager em;

	@Override
	@SuppressWarnings("unchecked")
	public List<CommentTbl> getListCommentByBlogId(String blogId) {
		List<CommentTbl> list = new ArrayList<>();
		try {
			list = em.createQuery("FROM CommentTbl A where blogId='"+blogId+"'").getResultList();
		} catch (Exception e) {
			return null;
		}
		return list;
	}

}
