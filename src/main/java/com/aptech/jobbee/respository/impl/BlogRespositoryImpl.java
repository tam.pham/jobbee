package com.aptech.jobbee.respository.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.aptech.jobbee.entity.BlogComment;
import com.aptech.jobbee.entity.BlogTbl;
import com.aptech.jobbee.respository.BlogRespository;


@Repository
public class BlogRespositoryImpl implements BlogRespository{
	
	@PersistenceContext
    EntityManager em;

	@Override
	public <S extends BlogTbl> Iterable<S> saveAll(Iterable<S> entities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<BlogTbl> findById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean existsById(Integer id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterable<BlogTbl> findAllById(Iterable<Integer> ids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void deleteById(Integer id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(BlogTbl entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll(Iterable<? extends BlogTbl> entities) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BlogTbl> getActiveBlog() {
		List<BlogTbl> list = new ArrayList<>();
		try {
			list = em.createQuery("FROM BlogTbl A where status = 1").getResultList();
		} catch (Exception e) {
			return null;
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BlogTbl findById(int id) {
		
		List<BlogTbl> list = new ArrayList<>();
		try {
			list = em.createQuery("FROM BlogTbl A where id = '"+id+"'").getResultList();
		} catch (Exception e) {
			return null;
		}
		return list.get(0);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override 
	public List<BlogComment> getTopComment() {
		List<BlogComment> result = new ArrayList<>();
		
		try {
			Query q = em.createQuery("Select blogId, count(*) as total FROM CommentTbl A group by blogId order by total DESC");
			List<Object> objects = q.getResultList();
			Iterator itr = objects.iterator();
			int i = 1;
			while(itr.hasNext()){
				if(i < 6) {
				Object[] obj = (Object[]) itr.next();
				   BlogComment blogComment = new BlogComment();
				   blogComment.setBlogId(String.valueOf(obj[0]));
				   blogComment.setTotalComment(String.valueOf(obj[1]));
				   result.add(blogComment);
				} else {
					break;
				}
			   i++;
			}
		} catch (Exception e) {
			return null;
		}
		return result;
	}

	@Override
	public List<BlogTbl> findAll() {
		List<BlogTbl> list = new ArrayList<>();
		try {
			list = em.createQuery("FROM BlogTbl A").getResultList();
		} catch (Exception e) {
			return null;
		}
		return list;
	}

	@Override
	public <S extends BlogTbl> S save(S entity) {
		em.persist(entity);
		return entity;
	}
	
}
