package com.aptech.jobbee.respository.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.aptech.jobbee.entity.UserTbl;
import com.aptech.jobbee.respository.UserRespository;


@Repository
public class UserRespositoryImpl implements UserRespository{
	
	@PersistenceContext
    EntityManager em;
	 
	@Override
	public <S extends UserTbl> S save(S entity) {
		em.persist(entity);
		return entity;
	}

	@Override
	public <S extends UserTbl> Iterable<S> saveAll(Iterable<S> entities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<UserTbl> findById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean existsById(Integer id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterable<UserTbl> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<UserTbl> findAllById(Iterable<Integer> ids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void deleteById(Integer id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(UserTbl entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll(Iterable<? extends UserTbl> entities) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public UserTbl checkLogin(String userName, String password) {
		try {
			UserTbl user = (UserTbl) em.createQuery("from UserTbl  where email = '"+userName+"' and password = '"+password+"'").getSingleResult();
			if ( user!= null) {
				return user;
			}
			return null;
		} catch (Exception e) {
			return null;
		}
		
	}

    @Override
    @SuppressWarnings("unchecked")
    public boolean isExitsEmail(String email) {
        try {
			List<UserTbl> list = em.createQuery("from UserTbl  where email = '"+email+"'").getResultList();
            if ( list.size() > 0) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

	@Override
	@SuppressWarnings("unchecked")
	public List<UserTbl> getAllUser() {
		List<UserTbl> list = new ArrayList<>();
		try {
			list = em.createQuery("from UserTbl").getResultList();
		} catch (Exception e) {
			return null;
		}
		return list;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<UserTbl> getAllUserByType(int userType) {
		List<UserTbl> list = new ArrayList<>();
		try {
			list = em.createQuery("from UserTbl where userType = '"+userType+"'").getResultList();
		} catch (Exception e) {
			return null;
		}
		return list;
	}

	@Override
	public UserTbl findById(int id) {
		try {
			UserTbl user = (UserTbl) em.createQuery("from UserTbl  where id = '"+id+"'").getSingleResult();
			if ( user!= null) {
				return user;
			}
		} catch (Exception e) {
			return null;
		}
		return null;
	}

}
