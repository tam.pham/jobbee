package com.aptech.jobbee.respository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.aptech.jobbee.entity.JobTypeTbl;
import com.aptech.jobbee.entity.LocationTbl;

@Repository
public interface JobTypeRespository extends CrudRepository<JobTypeTbl, Integer>{
	
}
