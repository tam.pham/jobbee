package com.aptech.jobbee.respository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.aptech.jobbee.entity.ContactTbl;

@Repository
public interface ContactRespository extends CrudRepository<ContactTbl, Long>{

}
