package com.aptech.jobbee.respository;

import org.springframework.stereotype.Repository;

import com.aptech.jobbee.entity.CompanyTbl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class CompanyRespository{
    @PersistenceContext
    EntityManager em;

    @Transactional
	public void createCompany(CompanyTbl companyTbl) {
		em.persist(companyTbl);
	}
    
    @Transactional
    public CompanyTbl getByCompanyName(String comName) {
        CompanyTbl company = (CompanyTbl) em.createQuery("from CompanyTbl where comName = '"+comName+"' ").getSingleResult();
        return company;
    }
    
    @Transactional
    public CompanyTbl getCompanyById(String id) {
        CompanyTbl company = em.find(CompanyTbl.class,Integer.parseInt(id));
        return company;
    }
    
    @Transactional
    public void updateCompany(CompanyTbl companyTbl) {
        em.merge(companyTbl);
    }

    @Transactional
    public List<CompanyTbl> findAll(){
        return em.createQuery("from CompanyTbl").getResultList();
    }
    
    @Transactional
    public void deleteCompany(String id) {
        em.remove(em.find(CompanyTbl.class,Integer.valueOf(id)));
    }
}
