package com.aptech.jobbee.respository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.aptech.jobbee.entity.JobPostTbl;

@Repository
public class JobPostRespository {
	@PersistenceContext
    EntityManager em;
	
	@Transactional
	public void createJob(JobPostTbl jobPostTbl) {
		em.persist(jobPostTbl);
	}

    @Transactional
    public void updateJob(JobPostTbl jobPostTbl) {
        em.merge(jobPostTbl);
    }
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<JobPostTbl> getAllJob() {
		List<JobPostTbl> listJob = em.createQuery("from JobPostTbl").getResultList();
		return listJob;
	}

    @Transactional
    public JobPostTbl getJobById(String id) {
        JobPostTbl jobPost = em.find(JobPostTbl.class,Integer.parseInt(id));
        return jobPost;
    }

    @Transactional
    public void deleteJob(String id) {
        em.remove(em.find(JobPostTbl.class,Integer.valueOf(id)));
    }
    
    @SuppressWarnings("unchecked")
	@Transactional
    public List<JobPostTbl> findJobPost(String jobTitle, String categoryName) {
    	List<JobPostTbl> listJob = new ArrayList<>();
    	if (StringUtils.isNotEmpty(categoryName)) {
    		listJob = em.createQuery("from JobPostTbl where jobTitle like '%"+jobTitle+"%' AND categoryName = '"+categoryName+"'").getResultList();
    	} else {
    		listJob = em.createQuery("from JobPostTbl where jobTitle like '%"+jobTitle+"%'").getResultList();
    	}
    			
    	return listJob;
    }
}
