package com.aptech.jobbee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JobbeeApplication {

	public static void main(String[] args) {
		SpringApplication.run(JobbeeApplication.class, args);
	}

}
