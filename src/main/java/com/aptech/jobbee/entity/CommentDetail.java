package com.aptech.jobbee.entity;

import java.io.Serializable;
import java.util.Date;

public class CommentDetail implements Serializable{

	private static final long serialVersionUID = 1L;

	private String email;
	private Date createAt;
	private String contents;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getCreateAt() {
		return createAt;
	}
	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}
	public String getContents() {
		return contents;
	}
	public void setContents(String contents) {
		this.contents = contents;
	}
	
}