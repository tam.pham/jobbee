package com.aptech.jobbee.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "seeker_salary")
public class SeekerSalaryTbl implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "current_salary")
	private double currentSalary;

	@Column(name = "currency")
	private char currency;

	@Column(name = "is_annually_month")
	private char isAnnuallyMonth;
	
	@Column(name = "profile_id")
	private int profileId;

	public char getCurrency() {
		return currency;
	}

	public void setCurrency(char currency) {
		this.currency = currency;
	}

	public int getProfileId() {
		return profileId;
	}

	public void setProfileId(int profileId) {
		this.profileId = profileId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getCurrentSalary() {
		return currentSalary;
	}

	public void setCurrentSalary(double currentSalary) {
		this.currentSalary = currentSalary;
	}

	public char getIsAnnuallyMonth() {
		return isAnnuallyMonth;
	}

	public void setIsAnnuallyMonth(char isAnnuallyMonth) {
		this.isAnnuallyMonth = isAnnuallyMonth;
	}

}
