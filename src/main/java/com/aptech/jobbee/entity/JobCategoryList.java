package com.aptech.jobbee.entity;

import java.io.Serializable;
import java.util.List;

public class JobCategoryList implements Serializable{

	private static final long serialVersionUID = 1L;

	private List<JobCategoryTbl> jobCategory;
	private int total;
	public List<JobCategoryTbl> getJobCategory() {
		return jobCategory;
	}
	public void setJobCategory(List<JobCategoryTbl> jobCategory) {
		this.jobCategory = jobCategory;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	
}