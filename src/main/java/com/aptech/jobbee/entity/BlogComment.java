package com.aptech.jobbee.entity;

import java.io.Serializable;

public class BlogComment implements Serializable{

	private static final long serialVersionUID = 1L;

	private String rank;
	private String title;
	private String totalComment;
	private String blogId;
	
	public String getRank() {
		return rank;
	}
	public void setRank(String rank) {
		this.rank = rank;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public void setTotalComment(String totalComment) {
		this.totalComment = totalComment;
	}
	public void setBlogId(String blogId) {
		this.blogId = blogId;
	}
	public String getTotalComment() {
		return totalComment;
	}
	public String getBlogId() {
		return blogId;
	}
	
}