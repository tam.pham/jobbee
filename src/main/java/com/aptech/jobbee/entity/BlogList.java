package com.aptech.jobbee.entity;

import java.io.Serializable;
import java.util.List;

public class BlogList implements Serializable{

	private static final long serialVersionUID = 1L;

	private List<BlogTbl> blogList;
	private int total;
	public List<BlogTbl> getBlogList() {
		return blogList;
	}
	public void setBlogList(List<BlogTbl> blogList) {
		this.blogList = blogList;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
}