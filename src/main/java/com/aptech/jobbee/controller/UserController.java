package com.aptech.jobbee.controller;

import java.io.IOException;
import java.util.Date;

import com.aptech.jobbee.Utils.GooglePojo;
import com.aptech.jobbee.Utils.GoogleUtils;
import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.aptech.jobbee.entity.UserTbl;
import com.aptech.jobbee.respository.UserRespository;
import com.aptech.jobbee.service.UserService;

import javax.servlet.http.HttpServletRequest;


@Controller
public class UserController {

    @Autowired
    private GoogleUtils googleUtils;

    @Autowired
	UserService userService;

	@GetMapping("/login")
	public String login() {
		return "login";
	}

	@GetMapping("/seeker-profile")
	public String getSeekerProfile() {
		return "seeker-profile";
	}

    @GetMapping("/seeker-dashboard")
    public String getSeekerDashboard() {
        return "seeker-dashboard";
    }

	@GetMapping("/employer-profile")
	public String getEmployerProfile() {
		return "employer-profile";
	}

    @RequestMapping("/login-google")
    public String loginGoogle(HttpServletRequest request) throws ClientProtocolException, IOException {
        String code = request.getParameter("code");

        if (code == null || code.isEmpty()) {
            return "redirect:/login?google=error";
        }

        String accessToken = googleUtils.getToken(code);

        GooglePojo googlePojo = googleUtils.getUserInfo(accessToken);
        UserDetails userDetail = googleUtils.buildUser(googlePojo);
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetail, null,
                userDetail.getAuthorities());
        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return "redirect:/";
    }

}
