package com.aptech.jobbee.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.aptech.jobbee.entity.BlogTbl;
import com.aptech.jobbee.entity.CommentTbl;
import com.aptech.jobbee.entity.UserTbl;
import com.aptech.jobbee.service.BlogService;
import com.aptech.jobbee.service.CommentService;

@Controller
public class CommentController {

	@Autowired
	private CommentService commentService;
	
	@Autowired
	private BlogService blogService;

	@PostMapping("/comment/add")
	public String addComment(@ModelAttribute CommentTbl commentTbl, @RequestParam(name="blogId") String blogId, HttpSession httpSession, ModelMap modelMap) {
		if (httpSession.getAttribute("user") != null){
            UserTbl user = (UserTbl) httpSession.getAttribute("user");
            if (user.getUserType() == 1){
                modelMap.addAttribute("userName",user.getEmail());
            }
            if (commentTbl != null) {
            	commentTbl.setCreateAt(new Date());
            	commentTbl.setUserId(user.getId());
            	commentTbl.setBlogId(blogId);
            	commentService.save(commentTbl);
            }
            
            BlogTbl blog = blogService.findById(Integer.parseInt(blogId));
    		if (blog != null) {
    			modelMap.put("blog", blog);
    		}
    		
    		List<CommentTbl> commentList = commentService.findByBlogId(blogId);
    		if (commentList.size() > 0) {
    			modelMap.put("comments", commentList);
    		}
        }
		return "blog-single";
	}
}