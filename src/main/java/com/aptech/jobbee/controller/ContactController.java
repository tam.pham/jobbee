package com.aptech.jobbee.controller;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ContactController {
	@Autowired
    private JavaMailSender sender;
	
	@GetMapping("contact")
	public String getContactPage() {
		return "contact";
	}
	
	@PostMapping("/contact")
    private void sendEmail(@RequestParam(value="name", required=false) String name, 
            @RequestParam(value="email", required=false) String email, 
            @RequestParam(value="comment", required=false) String comment) throws Exception{
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
         
        helper.setTo("ngduykhuong0503@gmail.com");
        helper.setText(comment);
        helper.setSubject(name + " | " + email);
         
        sender.send(message);
    }
}
