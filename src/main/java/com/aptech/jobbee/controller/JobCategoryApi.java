package com.aptech.jobbee.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.aptech.jobbee.entity.JobCategoryList;
import com.aptech.jobbee.entity.JobCategoryTbl;
import com.aptech.jobbee.service.JobCategoryApiService;

@RestController
@RequestMapping(value="api")
public class JobCategoryApi {

	@Autowired
	private JobCategoryApiService jobCategoryApiService;
	
	@GetMapping(value = "/jobCategory")
	 @CrossOrigin(origins = "http://localhost:8081")
	public ResponseEntity<JobCategoryList> findAllBlog() {
		
		JobCategoryList jobCategoryList = new JobCategoryList();
        List<JobCategoryTbl> jobCategorys = (List<JobCategoryTbl>) jobCategoryApiService.findAll();
        
        if (jobCategorys.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        
        jobCategoryList.setJobCategory(jobCategorys);
        jobCategoryList.setTotal(jobCategorys.size());
        return new ResponseEntity<>(jobCategoryList, HttpStatus.OK);
    }
	
	@GetMapping(value = "/jobCategory/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	 @CrossOrigin(origins = "http://localhost:8081")
    public ResponseEntity<JobCategoryTbl> getJobCategoryById(@PathVariable("id") Long id) {
        Optional<JobCategoryTbl> jobCategory = jobCategoryApiService.findById(id);

        if (!jobCategory.isPresent()) {
            return new ResponseEntity<>(new JobCategoryTbl(), HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(jobCategory.get(), HttpStatus.OK);
    }
	
	@PostMapping(value = "/jobCategory")
	 @CrossOrigin(origins = "http://localhost:8081")
    public ResponseEntity<JobCategoryTbl> createJobCategory(@RequestBody JobCategoryTbl jobCategoryTbl, UriComponentsBuilder builder) {
		
//		Optional<JobCategoryTbl> jobCategory = jobCategoryApiService.findById(jobCategoryTbl.getId());
//		
//		if (jobCategory.isPresent()) {
//			return new ResponseEntity<>(new JobCategoryTbl(), HttpStatus.CONFLICT);
//		}
//		
		jobCategoryApiService.save(jobCategoryTbl);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(builder.path("/jobCategory/{id}")
                .buildAndExpand(jobCategoryTbl.getId()).toUri());
        return new ResponseEntity<>(jobCategoryTbl, HttpStatus.CREATED);
    }
	
	@DeleteMapping(value = "/jobCategory/{id}")
	 @CrossOrigin(origins = "http://localhost:8081")
	public ResponseEntity<JobCategoryTbl> deleteJobCategory(@PathVariable("id") Long id) {
        Optional<JobCategoryTbl> jobCategory = jobCategoryApiService.findById(id);
        if (!jobCategory.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        jobCategoryApiService.remove(jobCategory.get());
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
	
	@PutMapping(value = "/jobCategory/{id}")
	@CrossOrigin(origins = "http://localhost:8081")
	public ResponseEntity<JobCategoryTbl> updateJobCategory(@PathVariable("id") Long id, @RequestBody JobCategoryTbl jobCategoryTbl) {
        Optional<JobCategoryTbl> jobCategory = jobCategoryApiService.findById(id);

        if (!jobCategory.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        jobCategory.get().setCategoryName(jobCategoryTbl.getCategoryName());

        jobCategoryApiService.save(jobCategory.get());
        return new ResponseEntity<>(jobCategory.get(), HttpStatus.OK);
    }
}
