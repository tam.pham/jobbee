package com.aptech.jobbee.controller;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.aptech.jobbee.entity.OrderDetailTbl;;

@Controller
public class OrderController {
	@GetMapping("manage-order")
	public String getManageOrder() {
		return "manage-order";
	}
	
	@GetMapping("order-product")
	public String getOrderProduct() {
		return "order-product";
	}
}
