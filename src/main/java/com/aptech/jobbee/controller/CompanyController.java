package com.aptech.jobbee.controller;

import com.aptech.jobbee.entity.UserTbl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.aptech.jobbee.entity.CompanyTbl;
import com.aptech.jobbee.service.CompanyService;
import com.aptech.jobbee.service.JobCategoryService;
import org.springframework.web.bind.annotation.SessionAttributes;
import javax.servlet.http.HttpSession;
import com.aptech.jobbee.service.LocationService;
@Controller
@SessionAttributes("user")
public class CompanyController {
	@Autowired
	CompanyService companyService;
	
	@Autowired
	LocationService locationService;
	
	@Autowired
	JobCategoryService jobCategoryService;

	@GetMapping("company-detail")
	public String getCompanyDetail(@RequestParam String id,HttpSession httpSession, ModelMap map) {
        if (httpSession.getAttribute("user") != null){
            UserTbl user = (UserTbl) httpSession.getAttribute("user");
            if (user.getUserType() == 1){
                map.addAttribute("userName",user.getEmail());
            }
        }
        map.addAttribute("companyDetail",companyService.getCompanyById(id));
		return "company-detail";
	}
	
	@GetMapping("manage-company")
	public String getManageCompany(ModelMap map) {
		map.addAttribute("companies", companyService.listCompany());
		return "manage-company";
	}
	
	@GetMapping("add-company")
	public String getAddCompany(ModelMap map) {
		map.addAttribute("locations", locationService.listLocation());
		map.addAttribute("jobcategories", jobCategoryService.listJobCategory());
		return "add-company";
	}
	
	@GetMapping("update-company")
    public String getUpdateCompany(@RequestParam String id,  ModelMap map) {
        map.addAttribute("company",companyService.getCompanyById(id));
        map.addAttribute("locations", locationService.listLocation());
        map.addAttribute("jobcategories", jobCategoryService.listJobCategory());
        return "update-company";
    }
	
	@GetMapping("delete-company")
    public String deleteCompany(@RequestParam String id) {
	    companyService.deleteCompany(id);
        return "redirect:/manage-company";
    }
	
	@GetMapping("list-company")
	public String getCompanys(HttpSession httpSession, ModelMap map) {
        if (httpSession.getAttribute("user") != null){
            UserTbl user = (UserTbl) httpSession.getAttribute("user");
            if (user.getUserType() == 1){
                map.addAttribute("userName",user.getEmail());
            }
        }
        map.addAttribute("listCompany", companyService.getAllCompany());
		return "list-company";
	}

	@PostMapping("add-company")
	public String addPost(@ModelAttribute CompanyTbl companyTbl) {
		companyService.createCompany(companyTbl);
        return "redirect:/manage-company";
	}
	
	@PostMapping("update-company")
    public String upDateCompany(@ModelAttribute CompanyTbl company) {
        companyService.updateCompany(company);
        return "redirect:/manage-company";
    }
}
