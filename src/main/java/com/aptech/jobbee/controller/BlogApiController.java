package com.aptech.jobbee.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import com.aptech.jobbee.entity.BlogComment;
import com.aptech.jobbee.entity.BlogList;
import com.aptech.jobbee.entity.BlogTbl;
import com.aptech.jobbee.service.BlogService;

@RestController
@RequestMapping(value="api")
public class BlogApiController {
	
	@Autowired
	private BlogService blogService;
	
	private static String PATH_UPLOAD = System.getProperty("user.dir") + "\\src\\main\\resources\\static\\assets\\images\\";
	
	@CrossOrigin(origins = "http://localhost:8081")
	@GetMapping(value = "/blog")
	public ResponseEntity<BlogList> findAllBlog() {
		BlogList blogList = new BlogList();
        List<BlogTbl> blogs = (List<BlogTbl>) blogService.findAll();
        if (blogs.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        blogList.setBlogList(blogs);
        blogList.setTotal(blogs.size());
        return new ResponseEntity<>(blogList, HttpStatus.OK);
    }
	 @CrossOrigin(origins = "http://localhost:8081")
	@GetMapping(value = "/blog/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BlogTbl> getBlogById(@PathVariable("id") Integer id) {
        BlogTbl blog = blogService.findById(id);

        if (blog == null) {
            return new ResponseEntity<>(new BlogTbl(), HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(blog, HttpStatus.OK);
    }
	
	@CrossOrigin(origins = "http://localhost:8081")
	@PostMapping(value = "/blog")
    public ResponseEntity<BlogTbl> createBlog(@RequestBody BlogTbl blogTbl, UriComponentsBuilder builder) {
//		String imageUrl = blogTbl.getImgUrl().replace("@/", "");
//		blogTbl.setImgUrl(imageUrl);
		blogTbl.setCreateAt(new Date());
		blogTbl.setUpdateAt(new Date());
		blogTbl.setStatus("1");
		
		blogService.save(blogTbl);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(builder.path("/blog/{id}")
                .buildAndExpand(blogTbl.getId()).toUri());
        return new ResponseEntity<>(blogTbl, HttpStatus.CREATED);
    }
	 @CrossOrigin(origins = "http://localhost:8081")
	@DeleteMapping(value = "/blog/{id}")
	public ResponseEntity<BlogTbl> deleteBlog(@PathVariable("id") Integer id) {
        BlogTbl blog = blogService.findById(id);
        if (blog == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        blogService.remove(blog);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
	
	@PutMapping(value = "/blog/{id}")
	@CrossOrigin(origins = "http://localhost:8081")
	public ResponseEntity<BlogTbl> updateBlog(@PathVariable("id") Integer id, @RequestBody BlogTbl blogTbl) {
        BlogTbl currentBlog = blogService.findById(id);

        if (currentBlog == null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        
        currentBlog.setStatus(blogTbl.getStatus());
        currentBlog.setTitle(blogTbl.getTitle());
        currentBlog.setContents(blogTbl.getContents());
        currentBlog.setUpdateAt(new Date());
        currentBlog.setUpdateAt(new Date());

        blogService.save(currentBlog);
        return new ResponseEntity<>(currentBlog, HttpStatus.OK);
    }
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping(value = "/blog/upload")
	@CrossOrigin(origins = "http://localhost:8081")
	public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile uploadfile) {
		
		if (uploadfile.isEmpty()) {
            return new ResponseEntity("please select a file!", HttpStatus.OK);
        }

        try {
            saveUploadedFiles(Arrays.asList(uploadfile));
        } catch (IOException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(uploadfile.getOriginalFilename(), HttpStatus.OK);
		
	}

	 private void saveUploadedFiles(List<MultipartFile> files) throws IOException {
		 String pathVueProject = System.getProperty("user.dir") + "\\vuejs\\public\\static\\images\\";
	        for (MultipartFile file : files) {

	            if (file.isEmpty()) {
	                continue; //next pls
	            }
	            byte[] bytes = file.getBytes();
	            Path path = Paths.get(PATH_UPLOAD + file.getOriginalFilename());
	            Path pathVue = Paths.get(pathVueProject + file.getOriginalFilename());
	            Files.write(path, bytes);
	            Files.write(pathVue, bytes);

	        }
	  }
	 
	 @GetMapping(value = "/blog-download")
	 public List<BlogComment> getTopComment() {
		 List<BlogComment> blogCommentList = blogService.getTopComment();
		 if(blogCommentList.size() > 0) {
			 for (int i = 0; i < blogCommentList.size(); i++) {
				BlogTbl blog = blogService.findById(Integer.parseInt(blogCommentList.get(i).getBlogId()));
				blogCommentList.get(i).setTitle(blog.getTitle());
				blogCommentList.get(i).setRank(String.valueOf(i+1));
			}
		 }
		 return blogCommentList;
	 }
}