package com.aptech.jobbee.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.aptech.jobbee.entity.UserTbl;
import com.aptech.jobbee.service.BlogService;
import com.aptech.jobbee.service.ContactService;
import com.aptech.jobbee.service.JobCategoryService;
import com.aptech.jobbee.service.JobPostService;
import com.aptech.jobbee.service.UserService;

@Controller
@SessionAttributes("user")
public class HomeController {
	@Autowired
	 BlogService blogService;
	
	@Autowired
    JobPostService jobPostService;
	
	@Autowired
	ContactService service;
	
	@Autowired
	UserService userService;
	
	@Autowired
	JobCategoryService jobCategoryService;
	
	@RequestMapping(value = { "/", "/home" }, method = RequestMethod.GET )
	public String homePage(Model map, HttpSession httpSession) {
	    if (httpSession.getAttribute("user") != null){
	        UserTbl user = (UserTbl) httpSession.getAttribute("user");
	        if (user.getUserType() == 1){
                map.addAttribute("userName",user.getEmail());
            }
	    }
	    map.addAttribute("listPost", jobPostService.getAllJob());
		map.addAttribute("listBlog",blogService.findAll());
		map.addAttribute("jobCategory", jobCategoryService.listJobCategory());
		return "homePage";
	}

	@RequestMapping(value = { "/job-page" }, method = RequestMethod.GET)
	public String contactusPage(ModelMap model) {
		model.addAttribute("address", "Vietnam");
		model.addAttribute("phone", "...");
		model.addAttribute("email", "...");
		return "job-page";
	}
	
}
