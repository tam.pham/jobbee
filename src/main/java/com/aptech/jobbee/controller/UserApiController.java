package com.aptech.jobbee.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.aptech.jobbee.entity.UserTbl;
import com.aptech.jobbee.service.UserService;

@RestController
@RequestMapping(value="api")
public class UserApiController {
    @Autowired
    UserService userService;

	@PostMapping("/login")
	public String authentication(@RequestParam String email, @RequestParam String password, HttpSession httpSession) {
        try {
            UserTbl result = userService.ckeckLogin(email, password);
            httpSession.setAttribute("user",result);
            if (result.getUserType() == 2) {
                return "employer";
            }
            return "seeker";
        } catch (Exception e) {
            return "false";
        }
	}

    @PostMapping("/register")
	public String register(@ModelAttribute UserTbl user) {
	    try {
            user.setIsActive(String.valueOf(1));
            user.setCreateDate(new Date());
            user.setUpdateDate(new Date());
            if (userService.isExistEmail(user.getEmail())){
                return "exits";
            }
            UserTbl returnValue = userService.createUser(user);
            if (returnValue != null){
                return "true";
            }
        } catch (Exception e){
            return "false";
        }
        return "false";
	}

    @PostMapping("/logout")
    public void logout(HttpSession httpSession) {
        httpSession.removeAttribute("user");
    }
}
