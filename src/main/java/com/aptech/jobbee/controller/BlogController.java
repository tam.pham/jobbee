package com.aptech.jobbee.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.aptech.jobbee.entity.BlogTbl;
import com.aptech.jobbee.entity.CommentTbl;
import com.aptech.jobbee.entity.UserTbl;
import com.aptech.jobbee.service.BlogService;
import com.aptech.jobbee.service.CommentService;
import com.aptech.jobbee.service.UserService;


@Controller
@SessionAttributes("user")
public class BlogController {
	
	@Autowired
	private BlogService blogService;
	
	@Autowired
	private CommentService commentService;
	
	@Autowired
	private UserService userService;
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping("/blog")
	public String getBlog(HttpServletRequest request, ModelMap modelMap, HttpSession httpSession) {
        if (httpSession.getAttribute("user") != null){
            UserTbl user = (UserTbl) httpSession.getAttribute("user");
            if (user.getUserType() == 1){
                modelMap.addAttribute("userName",user.getEmail());
            }
        }
	    List<BlogTbl> blogs = (List<BlogTbl>) blogService.getActiveBlog();
		PagedListHolder pagedListHolder = new PagedListHolder(blogs);
		int page = ServletRequestUtils.getIntParameter(request, "p", 0);
		pagedListHolder.setPage(page);
		pagedListHolder.setPageSize(3);
		pagedListHolder.setMaxLinkedPages(3);
		modelMap.put("blogList", pagedListHolder);
		return "blog";
	}
	
	@GetMapping(path="/blog-detail")
	public String getBlogSingle(@RequestParam(name="id") String id,@ModelAttribute CommentTbl commentTbl, @RequestParam(name="blogId") String blogId, ModelMap modelMap, HttpSession httpSession) {
		if (httpSession.getAttribute("user") != null){
            UserTbl user = (UserTbl) httpSession.getAttribute("user");
            if (user.getUserType() == 1){
                modelMap.addAttribute("userName",user.getEmail());
            }
            
            if (StringUtils.isNotEmpty(blogId)) {
    			if (commentTbl != null) {
                	commentTbl.setCreateAt(new Date());
                	commentTbl.setUserId(user.getId());
                	commentTbl.setBlogId(blogId);
                	commentService.save(commentTbl);
                }
    		}
        }
		if (StringUtils.isNotEmpty(id)) {
			BlogTbl blog = blogService.findById(Integer.parseInt(id));
			if (blog != null) {
				modelMap.put("blog", blog);
			}
		}
		
		List<CommentTbl> commentList = commentService.findByBlogId(id);
		if (commentList.size() > 0) {
			for(int i=0; i<commentList.size();i++) {
				UserTbl user1 = userService.findById(commentList.get(i).getUserId());
				commentList.get(i).setEmail(user1.getEmail());
			}
			modelMap.put("comments", commentList);
		}
		
		return "blog-single";
	}
	
	@PostMapping(path="/blog-detail")
	public String getBlogSingle(@ModelAttribute CommentTbl commentTbl, @RequestParam(name="blogId") String blogId, ModelMap modelMap, HttpSession httpSession) {
		if (httpSession.getAttribute("user") != null){
            UserTbl user = (UserTbl) httpSession.getAttribute("user");
            if (user.getUserType() == 1){
                modelMap.addAttribute("userName",user.getEmail());
            }
            
            if (StringUtils.isNotEmpty(blogId)) {
    			if (commentTbl != null) {
                	commentTbl.setCreateAt(new Date());
                	commentTbl.setUserId(user.getId());
                	commentTbl.setBlogId(blogId);
                	commentService.save(commentTbl);
                }
    		}
        }
		BlogTbl blog = blogService.findById(Integer.parseInt(blogId));
		if (blog != null) {
			modelMap.put("blog", blog);
		}
		
		List<CommentTbl> commentList = commentService.findByBlogId(blogId);
		if (commentList.size() > 0) {
			for(int i=0; i<commentList.size();i++) {
				UserTbl user1 = userService.findById(commentList.get(i).getUserId());
				commentList.get(i).setEmail(user1.getEmail());
			}
			modelMap.put("comments", commentList);
		}
		
		return "blog-single";
	}
	
	
}