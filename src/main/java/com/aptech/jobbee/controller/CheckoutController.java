//package com.aptech.jobbee.controller;
//
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.RequestMapping;
//
//@Controller
//public class CheckoutController {
//
//    private String stripePublicKey = "pk_test_83xvxe7E1jh7KePjr7VCkIRv00YekLN3Ze";
//
//    @RequestMapping("/checkout")
//    public String checkout(Model model) {
//        model.addAttribute("amount", 50 * 100); // in cents
//        model.addAttribute("stripePublicKey", stripePublicKey);
//        model.addAttribute("currency ", ChargeRequest.Currency.EUR);
//        return "checkout";
//    }
//}
