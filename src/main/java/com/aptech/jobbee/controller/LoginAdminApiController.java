package com.aptech.jobbee.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aptech.jobbee.entity.BlogTbl;
import com.aptech.jobbee.entity.UserTbl;
import com.aptech.jobbee.service.UserService;

@RestController
@RequestMapping(value="api")
public class LoginAdminApiController {

	@Autowired
	private UserService userService;

	@CrossOrigin(origins = "http://localhost:8081")
	@PostMapping(value = "/login/admin")
    public ResponseEntity<String> checkLogin(@RequestBody UserTbl userTbl) {
		try {
			if (userTbl != null) {
				UserTbl user = userService.ckeckLogin(userTbl.getEmail(), userTbl.getPassword());
				if (user != null) {
					return new ResponseEntity<>(HttpStatus.OK);
				}
			}

		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
	
	@CrossOrigin(origins = "http://localhost:8081")
	@PutMapping(value = "/user/update/{id}")
	public ResponseEntity<UserTbl> updateUserStatus(@PathVariable("id") int id, @RequestBody UserTbl userTbl) {
        UserTbl currentUser = userService.findById(id);

       if (currentUser == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

       currentUser.setIsActive(userTbl.getIsActive());
        userService.saveUser(currentUser);
        return new ResponseEntity<>(currentUser, HttpStatus.OK);
    }
	
	@CrossOrigin(origins = "http://localhost:8081")
	@GetMapping(value = "/user/all")
	public ResponseEntity<List<UserTbl>> getAllUser() {
		List<UserTbl> userList = userService.getAllUser();
		if(userList.size() > 0) {
			return new ResponseEntity<>(userList, HttpStatus.OK);
		}
		
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@CrossOrigin(origins = "http://localhost:8081")
	@GetMapping(value = "/user/all{user_type}")
	public ResponseEntity<List<UserTbl>> getAllUserByType(@PathVariable("user_type") int userType) {
		List<UserTbl> userList = userService.getAllUserByType(userType);
		if(userList.size() > 0) {
			return new ResponseEntity<>(userList, HttpStatus.OK);
		}
		
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	 @CrossOrigin(origins = "http://localhost:8081")
		@GetMapping(value = "/user/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	    public ResponseEntity<UserTbl> getUserById(@PathVariable("id") Integer id) {
	        UserTbl user = userService.findById(id);

	        if (user == null) {
	            return new ResponseEntity<>(new UserTbl(), HttpStatus.NO_CONTENT);
	        }
	        return new ResponseEntity<>(user, HttpStatus.OK);
	    }
		
	
}