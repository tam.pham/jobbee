package com.aptech.jobbee.controller;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.aptech.jobbee.entity.JobPostTbl;
import com.aptech.jobbee.entity.UserTbl;
import com.aptech.jobbee.service.CompanyService;
import com.aptech.jobbee.service.JobCategoryService;
import com.aptech.jobbee.service.JobPostService;


@Controller
@SessionAttributes("user")
public class JobController {
    @Autowired
    JobPostService jobPostService;

    @Autowired
    CompanyService companyService;
    
    @Autowired
	JobCategoryService jobCategoryService;

	/*
	 * @GetMapping("list-job") public String getJobList(ModelMap modelMap,
	 * HttpSession httpSession) { if (httpSession.getAttribute("user") != null){
	 * UserTbl user = (UserTbl) httpSession.getAttribute("user"); if
	 * (user.getUserType() == 1){ modelMap.addAttribute("userName",user.getEmail());
	 * } } modelMap.addAttribute("listPost", jobPostService.getAllJob());
	 * 
	 * return "list-job"; }
	 */
	
	@GetMapping("job-detail")
	public String getJobDetail(@RequestParam String id, ModelMap map, HttpSession httpSession) {
        if (httpSession.getAttribute("user") != null){
            UserTbl user = (UserTbl) httpSession.getAttribute("user");
            if (user.getUserType() == 1){
                map.addAttribute("userName",user.getEmail());
            }
        }
	    map.addAttribute("jobDetail",jobPostService.getJobById(id));
        map.addAttribute("company",companyService.getByCompanyName(jobPostService.getJobById(id).getCompany()));
		return "job-detail";
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping("list-job")
	public String findJobList(@ModelAttribute JobPostTbl jobPostTbl, ModelMap modelMap, HttpSession httpSession, HttpServletRequest request) {
        if (httpSession.getAttribute("user") != null){
            UserTbl user = (UserTbl) httpSession.getAttribute("user");
            if (user.getUserType() == 1){
                modelMap.addAttribute("userName",user.getEmail());
            }
        }
        
        List<JobPostTbl> jobPostList = new ArrayList<JobPostTbl>();
        if (StringUtils.isNotEmpty(jobPostTbl.getJobTitle()) || StringUtils.isNotEmpty(jobPostTbl.getCategoryName())) {
        	jobPostList = jobPostService.findJobPost(jobPostTbl.getJobTitle(), jobPostTbl.getCategoryName());
        } else {
        	jobPostList = jobPostService.getAllJob();
        }
        
        PagedListHolder pagedListHolder = new PagedListHolder(jobPostList);
		int page = ServletRequestUtils.getIntParameter(request, "p", 0);
		pagedListHolder.setPage(page);
		pagedListHolder.setPageSize(3);
		pagedListHolder.setMaxLinkedPages(3);
        modelMap.addAttribute("listPost", pagedListHolder);
        modelMap.addAttribute("jobCategory", jobCategoryService.listJobCategory());
        modelMap.addAttribute("jobTitle", jobPostTbl.getJobTitle());
        modelMap.addAttribute("categoryName", jobPostTbl.getCategoryName());
		return "list-job";
	}
	
}
