package com.aptech.jobbee.controller;

import java.io.FileOutputStream;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.pdf.PdfWriter;


@RestController
@RequestMapping(value="api")
public class ReportApiController {
	@GetMapping("report")
	public ResponseEntity<?> getReport() {
		try {
			Document document = new Document();
			  String PATH_UPLOAD = System.getProperty("user.dir") + "\\src\\main\\resources\\static\\assets\\";
			PdfWriter.getInstance(document, new FileOutputStream(PATH_UPLOAD+"report.pdf"));
			 
			document.open();
			Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
			Chunk chunk = new Chunk("Hello baby", font);
			 
			document.add(chunk);
			document.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
        return new ResponseEntity<>(1, HttpStatus.OK);
    }
}
