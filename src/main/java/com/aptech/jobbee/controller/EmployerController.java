package com.aptech.jobbee.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.aptech.jobbee.entity.JobPostTbl;
import com.aptech.jobbee.service.CompanyService;
import com.aptech.jobbee.service.JobCategoryService;
import com.aptech.jobbee.service.JobPostService;
import com.aptech.jobbee.service.JobTypeService;
import com.aptech.jobbee.service.LocationService;

@Controller
public class EmployerController {
	
	@Autowired
	LocationService locationService;
	
	@Autowired
	JobTypeService jobTypeService;
	
	@Autowired
	JobCategoryService jobCategoryService;

	@Autowired
	CompanyService companyService;
	
	@Autowired
	JobPostService jobPostService;
	
	
	@GetMapping("manage-post")
	public String getManagePost(ModelMap map) {
		map.addAttribute("listPost", jobPostService.getAllJob());
		return "manage-post";
	}
	
	
	@GetMapping("manage-application")
	public String getManageApplication() {
		return "manage-application";
	}
	
	
	@GetMapping("add-job")
	public String getAddPost(ModelMap map) {
		map.addAttribute("locations", locationService.listLocation());
		map.addAttribute("jobtypes", jobTypeService.listJobType());
		map.addAttribute("jobcategories", jobCategoryService.listJobCategory());
		map.addAttribute("companies", companyService.listCompany());
		return "add-job";
	}

    @GetMapping("update-job")
    public String getUpdatePost(@RequestParam String id,  ModelMap map) {
        map.addAttribute("jobPost",jobPostService.getJobById(id));
        map.addAttribute("locations", locationService.listLocation());
        map.addAttribute("jobtypes", jobTypeService.listJobType());
        map.addAttribute("jobcategories", jobCategoryService.listJobCategory());
        map.addAttribute("companies", companyService.listCompany());
        return "update-job";
    }

    @GetMapping("delete-job")
    public String deleteJob(@RequestParam String id) {
	    jobPostService.deleteJob(id);
        return "redirect:/manage-post";
    }

	@GetMapping("product-list")
	public String getProductList() {
		return "product-list";
	}
	
	@PostMapping("add-job")
	public String addPost(@ModelAttribute JobPostTbl jobPost) {
	    jobPost.setCreatedDate(String.valueOf(new Date()));
		jobPostService.createJobPost(jobPost);
        return "redirect:/manage-post";
	}

    @PostMapping("update-job")
    public String upDatePost(@ModelAttribute JobPostTbl jobPost) {
        jobPostService.updateJob(jobPost);
        return "redirect:/manage-post";
    }
}
