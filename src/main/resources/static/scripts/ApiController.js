$(document).ready(function () {
    var isValidEmail = false;
    var isValidPassword = false;
    /*Function validate email*/
    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

    function validateEmail(email){
        var emailVal = $(email).val();
        var idEmail = "#error-"+ email.substring(1,email.length);
        if (emailVal.length < 1) {
            $(idEmail).text("The Email field is required.");
            $(email).css("border","1px solid red");
            isValidEmail = false;
        } else if (!isEmail(emailVal)){
            $(idEmail).text("The Email must be a valid Email address.");
            $(email).css("border","1px solid red");
            isValidEmail = false;
        } else {
            $(idEmail).text("");
            $(email).css("border","1px solid #e0e0e0");
            isValidEmail = true;
        }
    }

    function validatePassword(password){
        var passwordVal = $(password).val();
        var idPassword = "#error-"+ password.substring(1,password.length);
        if (passwordVal.length < 1){
            $(idPassword).text("The password field is required.");
            $(password).css("border","1px solid red");
            isValidPassword = false;
        } else if (passwordVal.length < 6) {
            $(idPassword).text("The Password must be at least 6 characters.");
            $(password).css("border","1px solid red");
            isValidPassword = false;
        } else if (passwordVal.length >30) {
            $(idPassword).text("The Password must not be greater than 30 characters.");
            $(password).css("border","1px solid red");
            isValidPassword = false;
        } else {
            $(idPassword).text("");
            $(password).css("border","1px solid #e0e0e0");
            isValidPassword = true;
        }
    }
    function resetErrorField(email,password){
        var idEmail = "#error-"+ email.substring(1,email.length);
        var idPassword = "#error-"+ password.substring(1,password.length);
        //email
        $(idEmail).text("");
        $(email).css("border","1px solid #e0e0e0");
        //password
        $(idPassword).text("");
        $(password).css("border","1px solid #e0e0e0");

    }
    // API login
	$("#btnLogin").click(function() {
		var email = $("#email").val();
		var password = $("#password").val();
		validateEmail("#email");
		validatePassword("#password");
		if (isValidEmail &&  isValidPassword) {
            $.ajax({
                url:"/api/login",
                type: 'POST',
                data:{
                    email:email,
                    password:password
                },
                success:function (value) {
                    var currentLocation = window.location.href;
                    if (value == "employer"){
                        var employerLocation = currentLocation.replace("login","manage-post");
                        window.location = employerLocation;
                    } else if (value == "seeker") {
                        var homeLocation = currentLocation.replace("login","");
                        window.location = homeLocation;
                    }
                    else {
                        $("#error-message").text("Invalid email or password!");
                    }
                }

            });
        }
	});
    // end API login

    // API register account
    $("#btnRegister").click(function() {
        var email = $("#email2").val();
        var userType = $("#userType").val();
        var password = $("#password1").val();
        var rePassword = $("#password2").val();
        validateEmail("#email2");
        validatePassword("#password1");
        if (password !== rePassword){
            $("#password1").css("border","1px solid red");
            $("#password2").css("border","1px solid red");
            $("#error-pass").text("*** The specified password do not match.");
            return;
        } else {
            $("#password1").css("border","1px solid #e0e0e0");
            $("#password2").css("border","1px solid #e0e0e0");
            $("#error-pass").text("");
        };
        if (isValidEmail &&  isValidPassword) {
            $.ajax({
                url:"/api/register",
                type: 'POST',
                data:{
                    email:email,
                    userType: userType,
                    password:password
                },
                success:function (value) {
                    if (value == "exits"){
                        $("#error-email2").text("This email already exists.");
                    }
                    if (value === "true"){
                        $('#modal-confirm').modal('show');
                    }
                }
            });
        }
    });
    // end API register account
    // reset error filed
    $("#btnTab1").click(function () {
        resetErrorField("#email2","#password1");
    })
    $("#btnTab2").click(function () {
        resetErrorField("#email","#password");
    })

    $("#btnModalYes").click(function () {
        window.location = "login"
    });
    $("#btnModalNo").click(function () {
        var currentLocation = window.location.href;
        var homeLocation = currentLocation.replace("login","");
        window.location = homeLocation;
    });

    $("#btnLogout").click(function () {
        $.ajax({
            url:"/api/logout",
            type: 'POST',
            success:function (value) {

            }
        });
        var currentLocation = window.location.href;
        var home = currentLocation.replace(currentLocation,"http://localhost:8080/");
        window.location = home;
    });

    $("#btnForget").click(function () {
        $("#modal-forgetpass").modal('show');
    });

    $("#btnSubmitForget").click(function () {
        var emailVal = $("#email-reset").val();
        if (emailVal.length < 1) {
            $("#error-email-reset").text("The Email field is required.");
            $("#div-reset").css("border","1px solid red");
            isValidEmail = false;
        } else if (!isEmail(emailVal)){
            $("#error-email-reset").text("The Email must be a valid Email address.");
            $("#div-reset").css("border","1px solid red");
            isValidEmail = false;
        } else {
            $("#error-email-reset").text("");
            $("#div-reset").css("border","1px solid #e0e0e0");
            isValidEmail = true;
        }
    });
});









