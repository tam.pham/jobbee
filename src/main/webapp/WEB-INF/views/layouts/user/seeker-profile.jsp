<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<div id="dashboard" style="padding-top: 0px !important;">


    <!-- Content
    ================================================== -->
    <div class="dashboard-content" style="margin-left: 0px !important;">
        <div class="row">
            <div class="col-lg-4 col-md-12">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Account Informations</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Avatar -->
                        <div class="edit-profile-photo">
                            <img src="https://www.gravatar.com/avatar/00000000000000000000000000000000?d=mm&amp;s=300" alt="">
                            <div class="change-photo-btn">
                                <div class="photoUpload">
                                    <span><i class="fa fa-upload"></i> Upload Photo</span>
                                    <input type="file" class="upload" />
                                </div>
                            </div>
                        </div>

                        <!-- Details -->
                        <div class="my-profile">
                            <label>Email</label>
                            <input value="tom@example.com" type="text">
                        </div>

                        <button class="button margin-top-15">Change Password</button>

                    </div>
                </div>
            </div>
            <!-- Table-->
            <div class="col-lg-8 col-md-12">

                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Profile Details</h4>
                    <div class="dashboard-list-box-content">

                        <div class="submit-page">

                            <!-- Email -->
                            <div class="form">
                                <h5>Your Name</h5>
                                <input class="search-field" type="text" placeholder="Your full name" value=""/>
                            </div>

                            <!-- Email -->
                            <div class="form">
                                <h5>Your Email</h5>
                                <input class="search-field" type="text" placeholder="mail@example.com" value=""/>
                            </div>

                            <!-- Title -->
                            <div class="form">
                                <h5>Professional Title</h5>
                                <input class="search-field" type="text" placeholder="e.g. Web Developer" value=""/>
                            </div>

                            <!-- Location -->
                            <div class="form">
                                <h5>Location</h5>
                                <input class="search-field" type="text" placeholder="e.g. London, UK" value=""/>
                            </div>

                            <!-- Logo -->
                            <div class="form">
                                <h5>Photo <span>(optional)</span></h5>
                                <label class="upload-btn">
                                    <input type="file" multiple />
                                    <i class="fa fa-upload"></i> Browse
                                </label>
                                <span class="fake-input">No file selected</span>
                            </div>

                            <!-- Email -->
                            <div class="form">
                                <h5>Video <span>(optional)</span></h5>
                                <input class="search-field" type="text" placeholder="A link to a video about you" value=""/>
                            </div>
                        </div>

                    </div>
                </div>


                <div class="dashboard-list-box margin-top-30">
                    <h4>Education</h4>
                    <div class="dashboard-list-box-content with-padding">

                        <div class="form-inside">

                            <!-- Add Education -->
                            <div class="form boxed box-to-clone education-box">
                                <a href="#" class="close-form remove-box button"><i class="fa fa-close"></i></a>
                                <input class="search-field" type="text" placeholder="School Name" value=""/>
                                <input class="search-field" type="text" placeholder="Qualification(s)" value=""/>
                                <input class="search-field" type="text" placeholder="Start / end date" value=""/>
                                <textarea name="desc" id="desc" cols="30" rows="10" placeholder="Notes (optional)"></textarea>
                            </div>

                            <a href="#" class="button gray add-education add-box margin-top-10"><i class="fa fa-plus-circle"></i> Add Education</a>
                        </div>

                    </div>
                </div>


                <div class="dashboard-list-box margin-top-30">
                    <h4>Experience</h4>
                    <div class="dashboard-list-box-content with-padding">
                        <div class="form-inside">

                            <!-- Add Experience -->
                            <div class="form boxed box-to-clone experience-box">
                                <a href="#" class="close-form remove-box button"><i class="fa fa-close"></i></a>
                                <input class="search-field" type="text" placeholder="Employer" value=""/>
                                <input class="search-field" type="text" placeholder="Job Title" value=""/>
                                <input class="search-field" type="text" placeholder="Start / end date" value=""/>
                                <textarea name="desc1" id="desc1" cols="30" rows="10" placeholder="Notes (optional)"></textarea>
                            </div>

                            <a href="#" class="button gray add-experience add-box margin-top-10"><i class="fa fa-plus-circle"></i> Add Experience</a>
                        </div>


                    </div>
                </div>


                <a href="#" class="button margin-top-30">Preview <i class="fa fa-arrow-circle-right"></i></a>

            </div>
        </div>
    </div>
</div>
</div>
<!-- Scripts
================================================== -->
<script src="scripts/jquery-3.4.1.min.js"></script>
<script src="scripts/jquery-migrate-3.1.0.min.js"></script>
<script src="scripts/custom.js"></script>
<script src="scripts/jquery.superfish.js"></script>
<script src="scripts/jquery.themepunch.tools.min.js"></script>
<script src="scripts/jquery.themepunch.revolution.min.js"></script>
<script src="scripts/jquery.themepunch.showbizpro.min.js"></script>
<script src="scripts/jquery.flexslider-min.js"></script>
<script src="scripts/chosen.jquery.min.js"></script>
<script src="scripts/jquery.magnific-popup.min.js"></script>
<script src="scripts/waypoints.min.js"></script>
<script src="scripts/jquery.counterup.min.js"></script>
<script src="scripts/jquery.jpanelmenu.js"></script>
<script src="scripts/stacktable.js"></script>
<script src="scripts/slick.min.js"></script>



</body>

<!--    from   dashboard-add-resume.html by   /  ], Sat, 19 Oct 2019 20:00:46 GMT -->
</html>