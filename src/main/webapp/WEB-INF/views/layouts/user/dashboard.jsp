<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %></html>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->

<!--    from   dashboard-add-resume.html by   /  ], Sat, 19 Oct 2019 20:00:46 GMT -->
<head>

    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <title>  Jobbee</title>

    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../dashboard-2.html">
    <link rel="stylesheet" href="../css/colors.css">


    <script src="https://maps.google.com/maps/api/js?sensor=true"></script>
</head>

<body>
<div id="wrapper">



    <!-- Header
    ================================================== -->
    <header class="dashboard-header">
        <div class="container">
            <div class="sixteen columns">

                <!-- Logo -->
                <div id="logo">
                    <h1><a href="index-2.html"><img src="images/logo.png" alt="  Jobbee" /></a></h1>
                </div>

                <!-- Menu -->
                <nav id="navigation" class="menu">
                    <ul id="responsive">

                        <li><a href="index-2.html">Home</a>
                            <ul>
                                <li><a href="index-2.html">Home #1</a></li>
                                <li><a href="index-3.html">Home #2</a></li>
                                <li><a href="index-4.html">Home #3</a></li>
                                <li><a href="index-5.html">Home #4</a></li>
                                <li><a href="index-6.html">Home #5</a></li>
                            </ul>
                        </li>

                        <li><a href="#">Pages</a>
                            <ul>
                                <li><a href="job-page.html">Job Page</a></li>
                                <li><a href="job-page-alt.html">Job Page Alternative</a></li>
                                <li><a href="resume-page.html">Resume Page</a></li>
                                <li><a href="shortcodes.html">Shortcodes</a></li>
                                <li><a href="icons.html">Icons</a></li>
                                <li><a href="pricing-tables.html">Pricing Tables</a></li>
                                <li><a href="blog.html">Blog</a></li>
                                <li><a href="contact.html">Contact</a></li>
                            </ul>
                        </li>

                        <li><a href="#">Browse Listings</a>
                            <ul>
                                <li><a href="browse-jobs.html">Browse Jobs</a></li>
                                <li><a href="browse-categories.html">Browse Categories</a></li>
                                <li><a href="browse-resumes.html">Browse Resumes</a></li>
                            </ul>
                        </li>

                        <li><a href="#" id="current">Dashboard</a>
                            <ul>
                                <li><a href="dashboard.html">Dashboard</a></li>
                                <li><a href="dashboard-messages.html">Messages</a></li>
                                <li><a href="dashboard-manage-resumes.html">Manage Resumes</a></li>
                                <li><a href="dashboard-add-resume.html">Add Resume</a></li>
                                <li><a href="dashboard-job-alerts.html">Job Alerts</a></li>
                                <li><a href="dashboard-manage-jobs.html">Manage Jobs</a></li>
                                <li><a href="dashboard-manage-applications.html">Manage Applications</a></li>
                                <li><a href="dashboard-add-job.html">Add Job</a></li>
                                <li><a href="dashboard-my-profile.html">My Profile</a></li>
                            </ul>
                        </li>
                    </ul>


                    <ul class="responsive float-right">
                        <li><a href="dashboard.html"><i class="fa fa-cog"></i> Dashboard</a></li>
                        <li><a href="index-2.html"><i class="fa fa-lock"></i> Log Out</a></li>
                    </ul>

                </nav>

                <!-- Navigation -->
                <div id="mobile-navigation">
                    <a href="#menu" class="menu-trigger"><i class="fa fa-reorder"></i></a>
                </div>

            </div>
        </div>
    </header>
    <div class="clearfix"></div>