<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<head>

<!-- Basic Page Needs
================================================== -->
<meta charset="utf-8">
<title>Jobbee</title>

<!-- Mobile Specific Metas
================================================== -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- CSS
================================================== -->
<link rel="stylesheet" type="text/css" href='<c:url value="/css/bootstrap.min.css"/>'>
<link rel="stylesheet" type="text/css" href='<c:url value="/css/style.css"/>'>
<link rel="stylesheet" type="text/css" href='<c:url value="/css/colors.css"/>'>
    <style>
        .or-css::after {
            content: "------------------";
            color: grey;
        }
        .or-css::before {
            content: "------------------";
            color: grey;
        }
        .form-gap {
            padding-top: 70px;
        }
    </style>
</head>
<body>
<div class="div-container">
	<div class="container">
		<div class="my-account" style="margin-top: 55px !important">
			<ul class="tabs-nav">
				<li class=""><a href="#tab1" id="btnTab1">Login</a></li>
				<li><a href="#tab2" id="btnTab2">Register</a></li>
			</ul>
			<div class="tabs-container">
				<!-- Login -->

				<div class="tab-content" id="tab1" style="display: none;">
						<p class="form-row">
							<a href="https://accounts.google.com/o/oauth2/auth?scope=email&redirect_uri=http://localhost:8080/login-google&response_type=code
								&client_id=301976587883-8ctinbj7bf92g03gp8f38jul8n8olr7j.apps.googleusercontent.com&approval_prompt=force">
								<img alt="vdf" src="/images/bg/login-with-google-icon-3.jpg" style="height: 57px; width: 100%">
							</a>
						</p>
                        <p style="text-align: center;font-size: 16px" class="or-css">Or</p>
						<h6 style="color: #F04646 ; text-align: center" id="error-message"></h6>
						<p class="form-row form-row-wide">
							<label for="email">Email: <i
								class="ln ln-icon-Mail"></i> <input type="text"
								class="input-text" id="email" />
							</label>
						</p>
                        <p style="color: #F04646 ;" id="error-email"></p>
						<p class="form-row form-row-wide">
							<label for="password">Password: <i
								class="ln ln-icon-Lock-2"></i> <input class="input-text"
								type="password" id="password" />
							</label>
						</p>
                        <p style="color: #F04646 ;" id="error-password"></p>
						<p class="lost_password">
							<a href="#" id="btnForget">Forgot Password?</a>
						</p>
						<p class="form-row">
							<input type="submit" class="button border fw margin-top-10"
								name="login" value="Login" id="btnLogin"/>
						</p>
				</div>

				<!-- Register -->
				<div class="tab-content" id="tab2" style="display: none;">

                    <p class="form-row form-row-wide">
                        <label for="email2">Email : <i
                            class="ln ln-icon-Mail"></i> <input type="text"
                            class="input-text" id="email2"/>
                        </label>
                    </p>
                    <p style="color: #F04646 ;" id="error-email2"></p>
                    <div>
                        <label>User Type:</label>
                        <select data-placeholder="Full-Time" class="chosen-select-no-single" id="userType">
                            <option value="1">Seeker</option>
                            <option value="2">Employer</option>
                        </select>
                    </div>
                    <p class="form-row form-row-wide">
                        <label for="password1">Password: <i
                            class="ln ln-icon-Lock-2"></i> <input class="input-text"
                            type="password" id="password1" />
                        </label>
                    </p>
                    <p style="color: #F04646 ;" id="error-password1"></p>
                    <p class="form-row form-row-wide">
                        <label for="password2">Repeat Password: <i
                            class="ln ln-icon-Lock-2"></i> <input class="input-text"
                            type="password" name="password2" id="password2" />
                        </label>
                    </p>
                    <p style="color: #F04646 ;" id="error-pass"></p>
                    <p class="form-row">
                        <input type="submit" class="button border fw margin-top-10" name="register" value="Register" id="btnRegister"/>
                    </p>
                    <!-- Modal -->
                    <div class="modal fade"  role="dialog" id="modal-confirm" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <h4 style="text-align: center">Register Successfully</h4>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-success" id="btnModalYes" data-dismiss="modal">Yes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Modal -->
				</div>
                <%--modal forget pass--%>
                <div class="modal fade"  role="dialog" id="modal-forgetpass" >
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="col-md-8 col-md-offset-2">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="text-center">
                                            <h3><i class="fa fa-lock fa-4x"></i></h3>
                                            <h2 class="text-center">Forgot Password?</h2>
                                            <p>You can reset your password here.</p>
                                            <div class="panel-body">
                                                <div id="register-form" role="form" autocomplete="off" class="form" method="post">

                                                    <p style="color: #F04646 ;" id="error-email-reset"></p>
                                                    <div class="form-group" id="div-reset">
                                                        <div class="input-group" id="email-reset-div">
                                                            <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>
                                                            <input id="email-reset" placeholder="email address" class="form-control" type="email">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <input name="recover-submit" class="btn btn-lg btn-primary btn-block" value="Reset Password" type="submit" id="btnSubmitForget">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%--modal forget pass--%>
			</div>
		</div>
	</div>
</div>
</body>
</html>