<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

	<div id="wrapper">
		<!-- Header
================================================== -->
		<header class="dashboard-header">
			<div class="container">
				<div class="sixteen columns">

					<!-- Logo -->
					<div id="logo">
					<a href='<c:url value="/"/>' style="display: flex;
    align-items: flex-end;"><img src="images/bg/bee.png" style="width:50px; height:50px;" alt="  Jobbee" /><h1 style="margin-left:10px;">Jobbee</h1></a>
					</div>

					<!-- Menu -->
					<!-- <nav id="navigation" class="menu">
						<ul id="responsive">

							<li><a href="index-2.html">Home</a>
								<ul>
									<li><a href="index-2.html">Home #1</a></li>
									<li><a href="index-3.html">Home #2</a></li>
									<li><a href="index-4.html">Home #3</a></li>
									<li><a href="index-5.html">Home #4</a></li>
									<li><a href="index-6.html">Home #5</a></li>
								</ul>
							</li>

							<li><a href="#">Pages</a>
								<ul>
									<li><a href="job-page.html">Job Page</a></li>
									<li><a href="job-page-alt.html">Job Page Alternative</a></li>
									<li><a href="resume-page.html">Resume Page</a></li>
									<li><a href="shortcodes.html">Shortcodes</a></li>
									<li><a href="icons.html">Icons</a></li>
									<li><a href="pricing-tables.html">Pricing Tables</a></li>
									<li><a href="blog.html">Blog</a></li>
									<li><a href="contact.html">Contact</a></li>
								</ul>
							</li>

							<li><a href="#">Browse Listings</a>
								<ul>
									<li><a href="browse-jobs.html">Browse Jobs</a></li>
									<li><a href="browse-categories.html">Browse Categories</a></li>
									<li><a href="browse-resumes.html">Browse Resumes</a></li>
								</ul>
							</li>

							<li><a href="#" id="current">Dashboard</a>
								<ul>
									<li><a href="dashboard.html">Dashboard</a></li>
									<li><a href="dashboard-messages.html">Messages</a></li>
									<li><a href="dashboard-manage-resumes.html">Manage Resumes</a></li>
									<li><a href="dashboard-add-resume.html">Add Resume</a></li>
									<li><a href="dashboard-job-alerts.html">Job Alerts</a></li>
									<li><a href="dashboard-manage-jobs.html">Manage Jobs</a></li>
									<li><a href="dashboard-manage-applications.html">Manage Applications</a></li>
									<li><a href="dashboard-add-job.html">Add Job</a></li>
									<li><a href="dashboard-my-profile.html">My Profile</a></li>
								</ul>
							</li>
						</ul>


						<ul class="responsive float-right">
							<li><a href="dashboard.html"><i class="fa fa-cog"></i> Dashboard</a></li>
							<li><a href="index-2.html"><i class="fa fa-lock"></i> Log Out</a></li>
						</ul>

					</nav>

					Navigation
					<div id="mobile-navigation">
						<a href="#menu" class="menu-trigger"><i class="fa fa-reorder"></i></a>
					</div> -->

				</div>
			</div>
		</header>
		<div class="clearfix"></div>


		<!-- Titlebar
================================================== -->


		<!-- Titlebar
================================================== -->

		<!-- Dashboard -->
		<div id="dashboard">

			<!-- Navigation
	================================================== -->

			<!-- Responsive Navigation Trigger -->
			<a href="#" class="dashboard-responsive-nav-trigger"><i class="fa fa-reorder"></i> Dashboard Navigation</a>

				<div class="dashboard-nav">
				<div class="dashboard-nav-inner">

					<ul data-submenu-title="Management">
						<li class="active-submenu"><a>For Employers</a>
							<ul>
								<li><a href='<c:url value="/manage-post"/>'>Manage Jobs <span class="nav-tag">5</span></a></li>
								<li><a href='<c:url value="/manage-application"/>'>Manage Applications <span class="nav-tag">4</span></a>
								<li><a href='<c:url value="/manage-company"/>'>Manage Company <span class="nav-tag">4</span></a></li>
								<li><a href='<c:url value="/manage-order"/>'>Manage Order <span class="nav-tag">4</span></a></li>
								<li><a href='<c:url value="/add-job"/>'>Add Job</a></li>
							</ul>
						</li>

					</ul>

					<ul data-submenu-title="Account">
						<li><a href='<c:url value="/employer-profile"/>'>My Profile</a></li>
						<li><a href="#">Logout</a></li>
					</ul>

				</div>
			</div>
			<!-- Navigation / End -->


			<!-- Content
	================================================== -->
			<div class="dashboard-content">

				<!-- Titlebar -->
				<div id="titlebar">
					<div class="row">
						<div class="col-md-12">
							<h2>Employer profile</h2>
							<!-- Breadcrumbs -->
							<nav id="breadcrumbs">
								<ul>
									<li><a href="">Home</a></li>
									<li><a href="#">Dashboard</a></li>
									<li>Employer profile</li>
								</ul>
							</nav>
						</div>
					</div>
				</div>


				<div class="row">
			<!-- Profile -->
			<div class="col-lg-6 col-md-12">
				<div class="dashboard-list-box margin-top-0">
					<h4 class="gray">Profile Details</h4>
					<div class="dashboard-list-box-static">
						<!-- Avatar -->
						<div class="edit-profile-photo">
							<div class="change-photo-btn">
								<div class="photoUpload">
								    <span><i class="fa fa-upload"></i> Upload Photo</span>
								    <input type="file" class="upload" />
								</div>
							</div>
						</div>

						<!-- Details -->
						<div class="my-profile">

							<label>Your Name</label>
							<input value="Tom Perrin" type="text">
							<label>Email</label>
							<input value="tom@example.com" type="text">
						</div>
					</div>
				</div>
			</div>

			<!-- Change Password -->
			<div class="col-lg-6 col-md-12">
				<div class="dashboard-list-box margin-top-0">
					<h4 class="gray">Change Password</h4>
					<div class="dashboard-list-box-static">

						<!-- Change Password -->
						<div class="my-profile">
							<label class="margin-top-0">Current Password</label>
							<input type="password">

							<label>New Password</label>
							<input type="password">

							<label>Confirm New Password</label>
							<input type="password">

							<button class="button margin-top-15">Change Password</button>
						</div>

					</div>
				</div>
			</div>
			<!-- Copyrights -->
			<div class="col-md-12">
				<h4 >Company Information</h4>
					<div class="dashboard-list-box-static">
					
						<!-- Details -->
						<div class="my-profile">
						<div class="edit-profile-photo">
							<div class="change-photo-btn">
								<div class="photoUpload">
								    <span><i class="fa fa-upload"></i> Upload Photo</span>
								    <input type="file" class="upload" />
								</div>
							</div>
						</div>
							<label>Company Name</label>
							<input value="Tom Perrin" type="text">

							<label>Company Size</label>
							<input value="200" type="text">

							<label>Company Address</label>
							<select data-placeholder="Ho Chi Minh" class="chosen-select-no-single">
								<option value="1">Ho Chi Minh</option>
								<option value="2">Ha Noi</option>
							</select>
							
							<label>Company Industry</label>
							<select data-placeholder="Choose Industry" class="chosen-select" multiple>
									
									<option value="3">Sales</option>
									<option value="4">Software Development</option>
									<option value="5">IT- Hardware/ Networking</option>
									<option value="6">Customer Service Agents</option>
									<option value="7">Sales & Marketing Experts</option>
									<option value="8">Accountants & Consultants</option>
							</select>
							
							<label>Benefits</label>
							<select data-placeholder="Choose Benefits" class="chosen-select" multiple>
									<option value="1">Bonus</option>
									<option value="2">Laptop</option>
									<option value="3">Training</option>
							</select>
							
							<label>Contact Name</label>
							<input value="200" type="text">
							
							<label>Phone Number</label>
							<input value="0123456789" type="text">
							
							<label>Description</label>
							<textarea name="notes" id="notes" cols="10" rows="5">Description</textarea>

						</div>
					</div>
					<button class="button margin-top-15">Save Change</button>
			</div>
		</div>

	</div>
	<!-- Content / End -->


		</div>
		<!-- Dashboard / End -->


	</div>
	<!-- Wrapper / End -->