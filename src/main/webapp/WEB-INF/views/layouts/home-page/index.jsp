<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    	<!-- Banner
================================================== -->
	<form method="GET" action="/list-job">
		<div id="banner" class="with-transparent-header parallax background"
			style="background-image: url(images/bg/banner-home-02.jpg)" data-img-width="2000" data-img-height="1330"
			data-diff="300">
			<div class="container">
				<div class="sixteen columns">

					<div class="search-container">

						<!-- Form -->
						<h2>Find Job</h2>
						<input type="text" id="jobTitle" name="jobTitle" class="ico-01" placeholder="job title, keywords or company name" value="" style="display: inline-block;"/>
						<select name="categoryName" id="categoryName" style="width: 20%;height: 60px;display: inline-block;">
							<option value="" selected>select job category</option>
						    <c:forEach items="${jobCategory}" var="item">
						        <option value="${item.categoryName}">${item.categoryName}</option>
						    </c:forEach>
						</select>
						<button style="display: inline-block;"><i class="fa fa-search"></i></button>

						<!-- Announce -->
						<div class="announce">
							We’ve over <strong>15 000</strong> job offers for you!
						</div> 
					</div>

				</div>
			</div>
		</div>
	</form>

		<!-- Content
================================================== -->

		<!-- Categories -->
		<div class="container">
			<div class="sixteen columns">
				<h3 class="margin-bottom-20 margin-top-10">Popular Categories</h3>

				<!-- Popular Categories -->
				<div class="categories-boxes-container">

					<!-- Box -->
					<a href='<c:url value="/list-job"/>' class="category-small-box">
						<i class="ln ln-icon-Bar-Chart"></i>
						<h4>Accouting / Finance</h4>
						<span>32</span>
					</a>

					<!-- Box -->
					<a href='<c:url value="/list-job"/>' class="category-small-box">
						<i class="ln ln-icon-Car"></i>
						<h4>Automotive Jobs</h4>
						<span>76</span>
					</a>

					<!-- Box -->
					<a href='<c:url value="/list-job"/>' class="category-small-box">
						<i class="ln  ln-icon-Worker"></i>
						<h4>Construction / Facilities</h4>
						<span>31</span>
					</a>

					<!-- Box -->
					<a href='<c:url value="/list-job"/>' class="category-small-box">
						<i class="ln  ln-icon-Student-Female"></i>
						<h4>Education / Training</h4>
						<span>76</span>
					</a>

					<!-- Box -->
					<a href='<c:url value="/list-job"/>' class="category-small-box">
						<i class="ln ln-icon-Medical-Sign"></i>
						<h4>Healthcare</h4>
						<span>32</span>
					</a>

					<!-- Box -->
					<a href='<c:url value="/list-job"/>' class="category-small-box">
						<i class="ln ln-icon-Plates"></i>
						<h4>Restarant / Food Service</h4>
						<span>67</span>
					</a>

					<!-- Box -->
					<a href='<c:url value="/list-job"/>' class="category-small-box">
						<i class="ln ln-icon-Globe"></i>
						<h4>Transportation / Logistics</h4>
						<span>45</span>
					</a>

					<!-- Box -->
					<a href='<c:url value="/list-job"/>' class="category-small-box">
						<i class="ln   ln-icon-Laptop-3"></i>
						<h4>Telecommunication</h4>
						<span>96</span>
					</a>

				</div>

				<div class="clearfix"></div>
				<div class="margin-top-30"></div>

				<a href='<c:url value="/list-job"/>' class="button centered">Browse All Categories</a>
				<div class="margin-bottom-55"></div>
			</div>
		</div>


		<div class="container">

			<!-- Recent Jobs -->
			<div class="eleven columns">
				<div class="padding-right">
					<h3 class="margin-bottom-25">Recent Jobs</h3>
					<div class="listings-container">

						<!-- Listing -->
						  <c:forEach var="l" items="${listPost}" begin="0" end="3">
						<a href='<c:url value="/job-detail?id=${l.id}"/>' class="listing full-time">
							<div class="listing-logo">
								<img src="images/bg/job-list-logo-01.png" alt="">
							</div>
							<div class="listing-title">
								 <h4>${l.jobTitle} <span class="listing-date">${l.jobType}</span></h4>
								<ul class="listing-icons">
									<li><i class="ln ln-icon-Management"></i> ${l.company}</li>
									<li><i class="ln ln-icon-Map2"></i>Ho Chi Minh</li>
									<li><i class="ln ln-icon-Money-2"></i> ${l.salaryMin}-${l.salaryMax}</li>
									<li>
										<div class="listing-date new">new</div>
									</li>
								</ul>
							</div>
						</a>
							</c:forEach>

					</div> 

					<a href='<c:url value="/list-job"/>' class="button centered"><i class="fa fa-plus-circle"></i> Show More Jobs</a>
					<div class="margin-bottom-55"></div>
				</div>
			</div>

			<!-- Job Spotlight -->
			<div class="five columns">
				<h3 class="margin-bottom-5">Job Spotlight</h3>

				<!-- Navigation -->
				<div class="showbiz-navigation">
					<div id="showbiz_left_1" class="sb-navigation-left"><i class="fa fa-angle-left"></i></div>
					<div id="showbiz_right_1" class="sb-navigation-right"><i class="fa fa-angle-right"></i></div>
				</div>
				<div class="clearfix"></div>

				<!-- Showbiz Container -->
				<div id="job-spotlight" class="showbiz-container">
					<div class="showbiz" data-left="#showbiz_left_1" data-right="#showbiz_right_1" data-play="#showbiz_play_1">
						<div class="overflowholder">

							<ul>
								 <c:forEach var="l" items="${listPost}" begin="0" end="3">
								<li>
									<div class="job-spotlight">
									
						
							
									
										<a href='<c:url value="/job-detail?id=${l.id}"/>'>
											<h4>${l.jobTitle} <span class="part-time">${l.jobType}</span></h4>
										</a>
										<span><i class="fa fa-briefcase"></i> ${l.company}</span>
										<span><i class="fa fa-map-marker"></i>Ho Chi Minh</span>
										<span><i class="fa fa-money"></i>  ${l.salaryMin}-${l.salaryMax}/ hour</span>
										<p>${l.jobDescription}</p>
										<a href="#" class="button">Apply For This Job</a>
									
									</div>
								</li>
										</c:forEach>
						

							</ul>
							<div class="clearfix"></div>

						</div>
						<div class="clearfix"></div>
					</div>
				</div>

			</div>
		</div>


		<section class="fullwidth-testimonial margin-top-15">

			<!-- Info Section -->
			<div class="container">
				<div class="sixteen columns">
					<h3 class="headline centered">
						What Our Users Say 😍
						<span class="margin-top-25">We collect reviews from our users so you can get an honest opinion of what an
							experience with our website are really like!</span>
					</h3>
				</div>
			</div>
			<!-- Info Section / End -->

			<!-- Testimonials Carousel -->
			<div class="fullwidth-carousel-container margin-top-20">
				<div class="testimonial-carousel testimonials">

					<!-- Item -->
					<div class="fw-carousel-review">
						<div class="testimonial-box">
							<div class="testimonial">Capitalize on low hanging fruit to identify a ballpark value added activity to
								beta test. Override the digital divide with additional clickthroughs from DevOps. Nanotechnology
								immersion along the information highway will close.</div>
						</div>
						<div class="testimonial-author">
							<img src="images/bg/resumes-list-avatar-03.png" alt="">
							<h4>Tom Baker <span>HR Specialist</span></h4>
						</div>
					</div>

					<!-- Item -->
					<div class="fw-carousel-review">
						<div class="testimonial-box">
							<div class="testimonial">Bring to the table win-win survival strategies to ensure proactive domination. At
								the end of the day, going forward, a new normal that has evolved from generation is on the runway
								heading towards a streamlined cloud content.</div>
						</div>
						<div class="testimonial-author">
							<img src="images/bg/resumes-list-avatar-02.png" alt="">
							<h4>Jennie Smith <span>Jobseeker</span></h4>
						</div>
					</div>

					<!-- Item -->
					<div class="fw-carousel-review">
						<div class="testimonial-box">
							<div class="testimonial">Leverage agile frameworks to provide a robust synopsis for high level overviews.
								Iterative approaches to corporate strategy foster collaborative thinking to further the overall value
								proposition. Organically grow the holistic world view.</div>
						</div>
						<div class="testimonial-author">
							<img src="images/bg/resumes-list-avatar-01.png" alt="">
							<h4>Jack Paden <span>Jobseeker</span></h4>
						</div>
					</div>

				</div>
			</div>
			<!-- Testimonials Carousel / End -->

		</section>


		<!-- Flip banner -->
		<a href=#" class="flip-banner margin-bottom-55" data-background="images/bg/all-categories-photo.jpg"
			data-color="#26ae61" data-color-opacity="0.93">
			<div class="flip-banner-content">
				<h2 class="flip-visible">Step inside and see for yourself!</h2>
				<h2 class="flip-hidden">Get Started <i class="fa fa-angle-right"></i></h2>
			</div>
		</a>
		<!-- Flip banner / End -->


		<!-- Recent Posts -->
		<div class="container">
			<div class="sixteen columns">
				<h3 class="margin-bottom-25">Recent Posts</h3>
			</div>


					
					 	<c:forEach items="${listBlog}" var="item" begin="0" end="2">
							<div class="one-third column">
								<!-- Post #1 -->
								<div class="recent-post">
									<div class="recent-post-img"><a href="#"><img src="images/bg/recent-post-01.jpg" alt=""></a>
										<div class="hover-icon"></div>
									</div>
									<a href="#">
										<h4>${item.title}</h4>
									</a>
									<div class="meta-tags">
										<span>${item.createAt}</span>
										<span><a href="#">0 Comments</a></span>
									</div>
									<p>${item.contents}</p>
									<a href="/blog/detail?id=${item.id}" class="button">Read More</a>
								</div>
							</div>
					</c:forEach> 

			
			</div>

		</div>