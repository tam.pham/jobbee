<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!-- Titlebar
================================================== -->
<form method="GET" action="/list-job">
		<div id="banner" class="with-transparent-header parallax background"
			style="background-image: url(images/bg/banner-home-02.jpg)" data-img-width="2000" data-img-height="1330"
			data-diff="300">
			<div class="container">
				<div class="sixteen columns">

					<div class="search-container">
						<input id="categoryValue" name="categoryValue" value="${categoryName}" hidden/>
						<!-- Form -->
						<h2>Find Job</h2>
						<input type="text" id="jobTitle" name="jobTitle" class="ico-01" placeholder="job title, keywords or company name" value="${jobTitle}" style="display: inline-block;"/>
						<select name="categoryName" id="categoryName" style="width: 20%;height: 60px;display: inline-block;">
							<option value="" selected>select job category</option>
						    <c:forEach items="${jobCategory}" var="item">
						        <option value="${item.categoryName}">${item.categoryName}</option>
						    </c:forEach>
						</select>
						<button style="display: inline-block;"><i class="fa fa-search"></i></button>

						<!-- Announce -->
						<div class="announce">
							We’ve over <strong>15 000</strong> job offers for you!
						</div> 
					</div>

				</div>
			</div>
		</div>
	</form>


<!-- Content
================================================== -->
<div class="container">
    <!-- Recent Jobs -->
    <div class="eleven columns">
        <div class="padding-right">

           <%--  <form action="#" method="get" class="list-search">
                <button><i class="fa fa-search"></i></button>
                <input type="text" placeholder="job title, keywords or company name" value=""/>
                <div class="clearfix"></div>
            </form> --%>

			<jsp:useBean id="listPost" scope="request" type="org.springframework.beans.support.PagedListHolder" />
				<c:url value="list-job" var="pagedLink">
					<c:param name="p" value="~" />
				</c:url>
            <ul class="job-list full">
                <c:forEach var="l" items="${listPost.pageList}">
                    <li><a href='<c:url value="/job-detail?id=${l.id}"/>'>
                        <img src="images/job-list-logo-01.png" alt="">
                        <div class="job-list-content">
                            <h4>${l.jobTitle} <span class="full-time">${l.jobType}</span></h4>
                            <div class="job-icons">
                                <span><i class="fa fa-briefcase"></i> ${l.company}</span>
                                <span><i class="fa fa-map-marker"></i> Ho Chi Minh</span>
                                <span><i class="fa fa-money"></i> ${l.salaryMin}-${l.salaryMax}</span>
                            </div>
                            <p>${l.jobDescription}</p>
                        </div>
                    </a>
                        <div class="clearfix"></div>
                    </li>
                </c:forEach>
            </ul>
            <div class="clearfix"></div>

            <div class="pagination-container">
				<tg:paging pagedListHolder="${listPost}" pagedLink="${pagedLink}" />
			</div>

        </div>
    </div>


    <!-- Widgets -->
    <div class="five columns">

        <!-- Sort by -->
        <div class="widget">
            <h4>Sort by</h4>

            <!-- Select -->
            <select data-placeholder="Choose Category" class="chosen-select-no-single">
                <option selected="selected" value="recent">Newest</option>
                <option value="oldest">Oldest</option>
                <option value="expiry">Expiring Soon</option>
                <option value="ratehigh">Hourly Rate – Highest First</option>
                <option value="ratelow">Hourly Rate – Lowest First</option>
            </select>

        </div>

        <!-- Location -->
        <div class="widget">
            <h4>Location</h4>
            <form action="#" method="get">
                <input type="text" placeholder="State / Province" value=""/>
                <input type="text" placeholder="City" value=""/>

                <input type="text" class="miles" placeholder="Miles" value=""/>
                <label for="zip-code" class="from">from</label>
                <input type="text" id="zip-code" class="zip-code" placeholder="Zip-Code" value=""/><br>

                <button class="button">Filter</button>
            </form>
        </div>

        <!-- Job Type -->
        <div class="widget">
            <h4>Job Type</h4>

            <ul class="checkboxes">
                <li>
                    <input id="check-1" type="checkbox" name="check" value="check-1" checked>
                    <label for="check-1">Any Type</label>
                </li>
                <li>
                    <input id="check-2" type="checkbox" name="check" value="check-2">
                    <label for="check-2">Full-Time <span>(312)</span></label>
                </li>
                <li>
                    <input id="check-3" type="checkbox" name="check" value="check-3">
                    <label for="check-3">Part-Time <span>(269)</span></label>
                </li>
                <li>
                    <input id="check-4" type="checkbox" name="check" value="check-4">
                    <label for="check-4">Internship <span>(46)</span></label>
                </li>
                <li>
                    <input id="check-5" type="checkbox" name="check" value="check-5">
                    <label for="check-5">Freelance <span>(119)</span></label>
                </li>
            </ul>

        </div>

        <!-- Rate/Hr -->
        <div class="widget">
            <h4>Rate / Hr</h4>

            <ul class="checkboxes">
                <li>
                    <input id="check-6" type="checkbox" name="check" value="check-6" checked>
                    <label for="check-6">Any Rate</label>
                </li>
                <li>
                    <input id="check-7" type="checkbox" name="check" value="check-7">
                    <label for="check-7">$0 - $25 <span>(231)</span></label>
                </li>
                <li>
                    <input id="check-8" type="checkbox" name="check" value="check-8">
                    <label for="check-8">$25 - $50 <span>(297)</span></label>
                </li>
                <li>
                    <input id="check-9" type="checkbox" name="check" value="check-9">
                    <label for="check-9">$50 - $100 <span>(78)</span></label>
                </li>
                <li>
                    <input id="check-10" type="checkbox" name="check" value="check-10">
                    <label for="check-10">$100 - $200 <span>(98)</span></label>
                </li>
                <li>
                    <input id="check-11" type="checkbox" name="check" value="check-11">
                    <label for="check-11">$200+ <span>(21)</span></label>
                </li>
            </ul>

        </div>



    </div>
    <!-- Widgets / End -->


</div>


<!-- Footer
================================================== -->
<div class="margin-top-25"></div>
<script src="scripts/ApiController.js"></script>
<script>
$("#categoryName").val($("#categoryValue").val());
$("#categoryValue").val() = "";
</script>
