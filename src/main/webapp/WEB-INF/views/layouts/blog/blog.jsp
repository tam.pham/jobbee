<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

	<!-- Titlebar
================================================== -->

	 <div id="titlebar" class="single">
		<div class="container">

			<div class="sixteen columns">
				<h2>Blog</h2>
				<span>Keep up to date with the latest news</span>
				<a class="button" id="topCommentbtn">TOP COMMENT</a>
			</div>

		</div>
	</div> 
	<!-- Content
================================================== -->
	<div class="container">

		<!-- Blog Posts -->
		<div class="sixteen columns">
			<div class="padding-right">

				<!-- Post -->
				<div class="post-container">
				
					<jsp:useBean id="blogList" scope="request" type="org.springframework.beans.support.PagedListHolder" />
					<c:url value="/blog" var="pagedLink">
						<c:param name="p" value="~" />
					</c:url>
					<c:forEach items="${blogList.pageList}" var="item">
						<div class="post-img">
							<a href="/blog-detail?id=${item.id}&blogId="><img src="/assets/images/${item.imgUrl}"></a>
							<div class="hover-icon"></div>
						</div>
						<div class="post-content">
							<a href="#">
								<h3>${item.title}</h3>
							</a>
							<div class="meta-tags">
								<span class="date"><fmt:formatDate value="${item.createAt}" type="date" pattern="dd-MM-yyyy"/></span>
							</div>
							<p id="limitContents">${item.contents}</p>
							<a class="button" href="/blog-detail?id=${item.id}&blogId=">Read More</a>
						</div>
					</c:forEach>
				<div class="pagination-container">
				
				</div>
					<tg:paging pagedListHolder="${blogList}" pagedLink="${pagedLink}" />
				</div>
				
			</div>
		</div>
		<!-- Blog Posts / End -->
		


	</div>
	<div style="display:none">
	<table id="topComment" class="display" style="display:none">
      
       <!-- Header Table -->
       <thead>
            <tr>
                <th>Rank</th>
				<th>Title</th>
                <th>Number of comment</th>
            </tr>
        </thead>
    </table>
   </div> 
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.css"/>
 
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.60/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.60/pdfmake.min.js"></script>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jqc-1.12.3/dt-1.10.16/b-1.4.2/b-html5-1.4.2/datatables.min.css"/>
  
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jqc-1.12.3/dt-1.10.16/b-1.4.2/b-html5-1.4.2/datatables.min.js"></script>


<script>

$(document).ready(function (){ 
		$("#limitContents").each(function(i){
        var len=$(this).text().trim().length;
        if(len>100)
        {
            $(this).text($(this).text().substr(0,300)+'...');
        }
    });

		var table = $('#topComment').DataTable({
			"dom": 'Bfrtip',
	        "buttons": [
	        	'copyHtml5',
	            'excelHtml5',
	            'csvHtml5',
	            'pdfHtml5'
	        ],
			"sAjaxSource": "/api/blog-download",
			"sAjaxDataProp": "",
			"order": [[ 0, "asc" ]],
			"aoColumns": [
			    	{ "mData": "rank"},
		      		{ "mData": "title" },
				  	{ "mData": "totalComment" }
			]
			
	 });

	$( "#topCommentbtn" ).click(function() {
		$( ".buttons-pdf" ).click();
	});
		 
 });
</script>