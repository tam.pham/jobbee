<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<!-- Titlebar
================================================== -->
	<div id="titlebar" class="single">
		<div class="container">

			<div class="sixteen columns">
				<h2>Blog</h2>
				<span>Keep up to date with the latest news</span>
			</div>

		</div>
	</div>


	<!-- Content
================================================== -->
	<div class="container">

		<!-- Blog Posts -->
		<div class="sixteen columns">
			<div class="padding-right">

				<!-- Post -->
				<div class="post-container">
					<div class="post-img">
						<img src="/assets/images/${blog.imgUrl}" alt="">
					</div>
					<div class="post-content">
						<a href="#">
							<h3>${blog.title}</h3>
						</a>
						<div class="meta-tags">
							<span class="date"><fmt:formatDate value="${blog.createAt}" type="date" pattern="dd-MM-yyyy"/></span>
						</div>
						<div class="clearfix"></div>
						<div class="margin-bottom-25"></div>
						<p>${blog.contents}</p>

					</div>
				</div>

				<!-- Comments -->
				
					<section class="comments">
						<h4>
							Comments <span class="comments-amount"></span>
						</h4>
						<c:forEach items="${comments}" var="item">
								<ul>
									<li>
										<!-- <div class="avatar">
											<img src="/assets/72189582_10159639721071840_2873618856608268288_o.png" alt="" />
										</div> -->
										<div class="comment-content">
											<div class="arrow-comment"></div>
											<div class="comment-by">${item.email}<span class="date"><fmt:formatDate value="${item.createAt}" type="date" pattern="dd-MM-yyyy"/></span> 
												<!-- <a href="#" class="reply"><i class="fa fa-reply"></i> Reply</a> -->
											</div>
											<p>${item.contents}</p>
										</div>
									</li>
			
								</ul>
						</c:forEach>
					</section>

				<div class="clearfix"></div>
				<div class="margin-top-35"></div>


				<!-- Add Comment -->
				<h4 class="comment">Add Comment</h4>
				<div class="margin-top-20"></div>

				<!-- Add Comment Form -->
				<form method="POST" action="/blog-detail">
					<input id="blogId" name="blogId" value="${blog.id}" hidden/> 
					<%-- <input id="id" name="id" value="${blog.id}" hidden/>  --%>
					<fieldset>
						<div>
							<label>Comment: <span>*</span></label>
							<textarea cols="40" rows="3" id="contents" name="contents"></textarea>
						</div>

					</fieldset>

					<input type="submit" value="Add Comment" style="margin-top: 10px;">
					<div class="clearfix"></div>
					<div class="margin-bottom-20"></div>

				</form>

			</div>
		</div>
		<!-- Blog Posts / End -->

	</div>
</body>
</html>