<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <script src="scripts/jquery-3.4.1.min.js"></script>
    <script src="scripts/bootstrap.min.js"></script>
	<script src="scripts/jquery-migrate-3.1.0.min.js"></script>
	<script src="scripts/custom.js"></script>
	<script src="scripts/jquery.superfish.js"></script>
	<script src="scripts/jquery.themepunch.tools.min.js"></script>
	<script src="scripts/jquery.themepunch.revolution.min.js"></script>
	<script src="scripts/jquery.themepunch.showbizpro.min.js"></script>
	<script src="scripts/jquery.flexslider-min.js"></script>
	<script src="scripts/chosen.jquery.min.js"></script>
	<script src="scripts/jquery.magnific-popup.min.js"></script>
	<script src="scripts/waypoints.min.js"></script>
	<script src="scripts/jquery.counterup.min.js"></script>
	<script src="scripts/jquery.jpanelmenu.js"></script>
	<script src="scripts/stacktable.js"></script>
	<script src="scripts/slick.min.js"></script>
	<script src="scripts/headroom.min.js"></script>
	<script src="scripts/jquery-simple-validator.min.js"></script>
    <script src="scripts/ApiController.js"></script>
    <style>
    .error-field ,.error-salary {
    	color: red;
    }
    </style>
  <script>
  
  $(function() {
	    //Catch the forms submit event.
	    $("#form-post-job").on("submit", function(evt) {
	    	evt.preventDefault();
	        //Setup variables
	        var valid = false;
	        var cur = $("#minSa").val();
	        var max = $("#maxSa").val();
			
	        //Is the bid valid?
	        valid = (parseInt(max) > 0 && (parseInt(max) >= parseInt(cur)) ? true : false);
				
	        //If the bid is not valid...
	        if ( !valid ) {
	            //Stop the form from submitting
	            evt.preventDefault();
	            $(".error-salary").html('Please make sure your salary is greater than the previous').show();
	            //Provide an error message (sorry for te use of alert!)
	          
	            //Focus on our textbox
	            $("#maxSa").focus();
	            return false;
	        }
	       /*  else {
	            $(".error-salary").html('').hide();
		     /*    $("#form-post-job").submit(); */
		        } */
	        //If the form is valid, it will submit as normal.
	    });
	});
  </script>