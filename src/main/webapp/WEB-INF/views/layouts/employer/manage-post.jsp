<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="scripts/jquery-3.4.1.min.js"></script>
	<div id="wrapper">
		<!-- Header
================================================== -->
		<header class="dashboard-header">
			<div class="container">
				<div class="sixteen columns">

					<!-- Logo -->
					<div id="logo">
						<a href='<c:url value="/"/>' style="display: flex;
    align-items: flex-end;"><img src="images/bg/bee.png" style="width:50px; height:50px;" alt="  Jobbee" /><h1 style="margin-left:10px;">Jobbee</h1></a>
					</div>

				

				</div>
			</div>
		</header>
		<div class="clearfix"></div>


		<!-- Titlebar
================================================== -->
<!-- Dashboard -->
<div id="dashboard">

	<!-- Navigation
	================================================== -->

	<!-- Responsive Navigation Trigger -->
			<a href="#" class="dashboard-responsive-nav-trigger"><i class="fa fa-reorder"></i> Dashboard Navigation</a>

				<div class="dashboard-nav">
				<div class="dashboard-nav-inner">

					<ul data-submenu-title="Management">
						<li class="active-submenu"><a>For Employers</a>
							<ul>
								<li><a href='<c:url value="/manage-post"/>'>Manage Jobs </a></li>
								<li><a href='#'>Manage Applications <span class="nav-tag">4</span></a>
								<li><a href='<c:url value="/manage-company"/>'>Manage Company </a></li>
								<li><a href='<c:url value="/manage-order"/>'>Manage Order</a></li>
								<li><a href='<c:url value="/add-job"/>'>Add Job</a></li>
							</ul>
						</li>

					</ul>

					<ul data-submenu-title="Account">
						<li><a href='<c:url value="/employer-profile"/>'>My Profile</a></li>
						<li><a href="#">Logout</a></li>
					</ul>

				</div>
			</div>
			<!-- Navigation / End -->


	<!-- Content
	================================================== -->
	<div class="dashboard-content">

		<!-- Titlebar -->
		<div id="titlebar">
			<div class="row">
				<div class="col-md-10">
					<h2>Manage Resumes</h2>
					<!-- Breadcrumbs -->
					<nav id="breadcrumbs">
						<ul>
							<li><a href="#">Home</a></li>
							<li><a href="#">Dashboard</a></li>
							<li>Manage Resumes</li>
						</ul>
					</nav>
				</div>
				<div class="col-md-2">
					<a class="button" id="listJobbtn">EXPORT</a>
				</div>
			</div>
		</div>


		<div class="row">

			<!-- Table-->
			<div class="col-lg-12 col-md-12">

				<div class="notification notice">
					Your listings are shown in the table below. Expired listings will be automatically removed after 30 days.
				</div>

				<div class="dashboard-list-box margin-top-30">
					<div class="dashboard-list-box-content">

						<!-- Table -->

						<table class="manage-table responsive-table">

							<tr>
								<th><i class="fa fa-file-text"></i> Title</th>
								<th><i class="fa fa-check-square-o"></i>Location</th>
								<th><i class="fa fa-calendar"></i> Date Posted</th>
								<th><i class="fa fa-calendar"></i> Date Updated</th>
								
								<th></th>
							</tr>

							<!-- Item #1 -->
							<c:forEach var="l" items="${listPost}">
							<tr>
								<td class="title">${l.jobTitle}</td>
								<td>Ho Chi Minh</td>
								<td>${l.createdDate}</td>
								<td class="centered">${l.createdDate}</td>
                                <td></td>
								<td class="action">
                                    <a href="<c:url value="/update-job?id=${l.id}"/>"><i class="fa fa-pencil"></i> Edit</a>
									<a href="<c:url value="/delete-job?id=${l.id}"/>"></i> Delete</a>
								</td>
							</tr>
							</c:forEach>


						</table>

					</div>
				</div>
				<a href="<c:url value="/add-job"/>" class="button margin-top-30">Add New Job</a>
			</div>


			<!-- Copyrights -->
			<div class="col-md-12">
				<div class="copyrights">© 2019 WorkScout. All Rights Reserved.</div>
			</div>
		</div>

	</div>
	<!-- Content / End -->
<div style="display:none">
	<table id="listPost" class="display" style="display:none">
      
       <!-- Header Table -->
       <thead>
            <tr>
                <th>Id</th>
				<th>User name</th>
                <th>Email</th>
            </tr>
        </thead>
    </table>
   </div> 

</div>
<!-- Dashboard / End -->

