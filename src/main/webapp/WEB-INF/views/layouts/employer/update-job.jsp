<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

	<div id="wrapper">
		<!-- Header
================================================== -->
		<header class="dashboard-header">
			<div class="container">
				<div class="sixteen columns">

					<!-- Logo -->
					<div id="logo">
						<a href='<c:url value="/"/>' style="display: flex;
    align-items: flex-end;"><img src="images/bg/bee.png" style="width:50px; height:50px;" alt="  Jobbee" /><h1 style="margin-left:10px;">Jobbee</h1></a>
					</div>


				</div>
			</div>
		</header>
		<div class="clearfix"></div>


		<!-- Dashboard -->
		<div id="dashboard">

			<!-- Navigation
	================================================== -->

			<!-- Responsive Navigation Trigger -->
			<a href="#" class="dashboard-responsive-nav-trigger"><i class="fa fa-reorder"></i> Dashboard Navigation</a>

				<div class="dashboard-nav">
				<div class="dashboard-nav-inner">

					<ul data-submenu-title="Management">
						<li class="active-submenu"><a>For Employers</a>
							<ul>
								<li><a href='<c:url value="/manage-post"/>'>Manage Jobs </a></li>
								<li><a href='<c:url value="/manage-application"/>'>Manage Applications</a>
								<li><a href='<c:url value="/manage-company"/>'>Manage Company</a></li>
								<li><a href='<c:url value="/manage-order"/>'>Manage Order </a></li>
								<li><a href='<c:url value="/add-job"/>'>Add Job</a></li>
							</ul>
						</li>

					</ul>

					<ul data-submenu-title="Account">
						<li><a href='<c:url value="/employer-profile"/>'>My Profile</a></li>
						<li><a href="#">Logout</a></li>
					</ul>

				</div>
			</div>
			<!-- Navigation / End -->


	<!-- Content
	================================================== -->
	<div class="dashboard-content">


		<!-- Titlebar -->
		<div id="titlebar">
			<div class="row">
				<div class="col-md-12">
					<h2>Update Job</h2>
					<!-- Breadcrumbs -->
					<nav id="breadcrumbs">
						<ul>
							<li><a href="#">Home</a></li>
							<li><a href="#">Dashboard</a></li>
							<li>Update Job</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>

		<div class="row">

			<!-- Table-->
			<div class="col-lg-12 col-md-12">

				<div class="dashboard-list-box margin-top-0">
					<form method="post"  action="/update-job" validate="true" id="form-post-job">
					<h4>Job Details</h4>
					<div class="dashboard-list-box-content">
					<div class="submit-page">

						<div class="form">
							<h5>Company</h5>
							<select data-placeholder="Choose Company" class="chosen-select" name="company" style="padding: 14px 18px;">
									<c:forEach var="l" items="${companies}">
									<option value="${l.comName}" ${l.comName == jobPost.company ? 'selected="selected"' : ''}>${l.comName}</option>
								</c:forEach>
								</select>
						</div>
						<!-- Email -->
						<div class="form">
							<h5>Your Email</h5>
							<input class="search-field"  type="email" placeholder="mail@example.com" required value="" name="email"  maxlength="255"/>
						</div>

						<!-- Title -->
						<div class="form">
							<h5>Job Title</h5>
							<input class="search-field" type="text" placeholder="" value="" name="jobTitle" maxlength="255" required minlength="10"/>
						</div>

						<!-- Job Type -->
						<div class="form">
							<h5>Job Type</h5>
							<select data-placeholder="Full-Time" class="chosen-select-no-single" name="jobType" style="padding: 14px 18px;">
								<c:forEach var="l" items="${jobtypes}">
									<option value="${l.typeName}" ${l.typeName == jobPost.jobType ? 'selected="selected"' : ''}>${l.typeName}</option>
								</c:forEach>
							</select>
						</div>


						<!-- Choose Category -->
						<div class="form">
							<div class="select">
								<h5>Category</h5>
								<select data-placeholder="Choose Categories" class="chosen-select" name="categoryName" style="padding: 14px 18px;"> 
									<c:forEach var="l" items="${jobcategories}">
									<option value="${l.categoryName}" ${l.categoryName == jobPost.categoryName ? 'selected="selected"' : ''}>${l.categoryName}</option>
								</c:forEach>
								</select>
							</div>
						</div>


						<div class="form">
							<h5>Salary From </h5>
							<input type="number" min="0"  max="5000" step="10" id="minSa" placeholder="" name="salaryMin" value="${jobPost.salaryMin}">
                            <h5>Salary To </h5>
                         <input type="number" min="0" max="10000" step="10" placeholder="" id="maxSa" name="salaryMax" value="${jobPost.salaryMax}">
                         	<p class="error-salary"></p>
						</div>

						<!-- Description -->
						<div class="form" style="width: 100%;">
							<h5>Description</h5>
							<textarea name="jobDescription" class="WYSIWYG" cols="1" rows="1" id="summary" spellcheck="true">${jobPost.jobDescription}</textarea>
						</div>

                        <!-- Description -->
                        <div class="form" style="width: 100%;">
                            <h5>Job Requirement</h5>
                            <textarea name="jobRequirement" class="WYSIWYG" cols="1" rows="1" id="summary" spellcheck="true">${jobPost.jobRequirement}</textarea>
                        </div>
                        <input value="${jobPost.id}" name="id" style="display: none"/>
                        <input value="${jobPost.createdDate}" name="createdDate" style="display: none"/>
						<!-- TClosing Date -->
						<div class="form">
							
						</div>

					</div>

					</div>
				</div>

				<input type="submit" class="button border fw margin-top-10" name="login" value="Update job" />
				</form>
			</div>


		</div>

	</div>
	<!-- Content / End -->


</div>
<!-- Dashboard / End -->
