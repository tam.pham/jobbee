<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
  <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    
<!-- Titlebar
================================================== -->
<div id="titlebar" class="single">
	<div class="container">

		<div class="sixteen columns">
			<h2>Package List</h2>
			<nav id="breadcrumbs">
				<ul>
					<li>You are here:</li>
					<li><a href="#">Home</a></li>
					<li>Package List</li>
				</ul>
			</nav>
		</div>

	</div>
</div>



<!-- Pricing Tables
================================================== -->

<!-- Container / Start -->
<div class="container">

	<div class="sixteen columns">
		<div class="notification notice margin-bottom-30">
			<p>Choose the package you want to subscription.</p>
		</div>
	</div>


<!-- Container / Start -->
<div class="container">

	<div class="sixteen columns">
		<h3 class="margin-bottom-20"></h3>
	</div>

	<!-- Plan #1 -->
	<div class="plan color-1 one-third column">
		<div class="plan-price">
			<h3>Start Up</h3>
			<span class="plan-currency">$</span>
			<span class="value">19</span>

		</div>
		<div class="plan-features">
			<ul>
				<li>One Time Fee</li>
				<li>This Plan Includes 1 Job</li>
				<li>Non-Highlighted Post</li>
				<li>Posted For 30 Days</li>
			</ul>
			<a class="button" href="#"><i class="fa fa-shopping-cart"></i> Add to Cart</a>
		</div>
	</div>

	<!-- Plan #2 -->
	<div class="plan color-2 one-third column">
		<div class="plan-price">
			<h3>Company</h3>
			<span class="plan-currency">$</span>
			<span class="value">59</span>
		</div>
		<div class="plan-features">
			<ul>
				<li>One Time Fee</li>
				<li>This Plan Includes 2 Jobs</li>
				<li>Highlighted Job Post</li>
				<li>Posted For 60 Days</li>
			</ul>
			<a class="button" href="#"><i class="fa fa-shopping-cart"></i> Add to Cart</a>
		</div>
	</div>

	<!-- Plan #3 -->
	<div class="plan color-1 one-third column">
		<div class="plan-price">
			<h3>Enterprise</h3>
			<span class="plan-currency">$</span>
			<span class="value">99</span>
		</div>
		<div class="plan-features">
			<ul>
				<li>One Time Fee</li>
				<li>This Plan Includes 4 Jobs</li>
				<li>2 Highlighted Job Posts</li>
				<li>Posted For 90 Days</li>
			</ul>
			<a class="button" href="#"><i class="fa fa-shopping-cart"></i> Add to Cart</a>
		</div>
	</div>

</div>