<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
  <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    
	<div id="wrapper">


		<!-- Header
================================================== -->
		<header class="sticky-header">
			<div class="container">
				<div class="sixteen columns">

					<!-- Logo -->
					<div id="logo">
						<h1><a href="index-2.html"><img src="images/logo.png" alt="  Jobbee" /></a></h1>
					</div>

					<!-- Menu -->
					<nav id="navigation" class="menu">
						<ul id="responsive">

							<li><a href="index-2.html">Home</a>
								<ul>
									<li><a href="index-2.html">Home #1</a></li>
									<li><a href="index-3.html">Home #2</a></li>
									<li><a href="index-4.html">Home #3</a></li>
									<li><a href="index-5.html">Home #4</a></li>
									<li><a href="index-6.html">Home #5</a></li>
								</ul>
							</li>

							<li><a id="current" href="#">Pages</a>
								<ul>
									<li><a href="job-page.html">Job Page</a></li>
									<li><a href="job-page-alt.html">Job Page Alternative</a></li>
									<li><a href="resume-page.html">Resume Page</a></li>
									<li><a href="shortcodes.html">Shortcodes</a></li>
									<li><a href="icons.html">Icons</a></li>
									<li><a href="pricing-tables.html">Pricing Tables</a></li>
									<li><a href="blog.html">Blog</a></li>
									<li><a href="contact.html">Contact</a></li>
								</ul>
							</li>

							<li><a href="#">Browse Listings</a>
								<ul>
									<li><a href="browse-jobs.html">Browse Jobs</a></li>
									<li><a href="browse-resumes.html">Browse Resumes</a></li>
									<li><a href="browse-categories.html">Browse Categories</a></li>
								</ul>
							</li>

							<li><a href="#">Dashboard</a>
								<ul>
									<li><a href="dashboard.html">Dashboard</a></li>
									<li><a href="dashboard-messages.html">Messages</a></li>
									<li><a href="dashboard-manage-resumes.html">Manage Resumes</a></li>
									<li><a href="dashboard-add-resume.html">Add Resume</a></li>
									<li><a href="dashboard-job-alerts.html">Job Alerts</a></li>
									<li><a href="dashboard-manage-jobs.html">Manage Jobs</a></li>
									<li><a href="dashboard-manage-applications.html">Manage Applications</a></li>
									<li><a href="dashboard-add-job.html">Add Job</a></li>
									<li><a href="dashboard-my-profile.html">My Profile</a></li>
								</ul>
							</li>
						</ul>


						<ul class="responsive float-right">
							<li><a href="order-product.html"><i class="ln ln-icon-Add-Cart"></i> My cart</a></li>
							<li><a href="my-account.html"><i class="fa fa-lock"></i> Log In</a></li>
						</ul>

					</nav>

					<!-- Navigation -->
					<div id="mobile-navigation">
						<a href="#menu" class="menu-trigger"><i class="fa fa-reorder"></i> Menu</a>
					</div>

				</div>
			</div>
		</header>
		<div class="clearfix"></div>

		<!-- Titlebar
================================================== -->
		<div id="titlebar" class="single">
			<div class="container">

				<div class="sixteen columns">
					<h2>Product Post Jobs</h2>
					<nav id="breadcrumbs">
						<ul>
							<li>You are here:</li>
							<li><a href="#">Home</a></li>
							<li>Product Post Jobs</li>
						</ul>
					</nav>
				</div>

			</div>
		</div>



		<!-- Pricing Tables
================================================== -->

		<!-- Container / Start -->
		<div class="container">

			<div class="sixteen columns">
				<div class="notification notice margin-bottom-30">
					<p>Checking your order and choose the payment method.</p>
				</div>
			</div>


			<!-- Container / Start -->
			<div class="container">

				<div class="sixteen columns">
					<!-- Accordion -->
					<div class="accordion">

						<!-- Section 1 -->
						<h3><span class="ui-accordion-header-icon ui-icon ui-accordion-icon"></span>1. Checking your order</h3>
						<div>
							<ul style="display: flex;justify-content: space-around; align-items: center;">
								<li>
									<h4>
										10 days post job
									</h4>
									<p>description here</p>
								</li>
								<li><input type="number" min="1" value="1"></li>
								<li style="font-size: 2rem;">$99</li>
								<li><a href="#"><i style="font-size: 2rem;" class="ln ln-icon-Remove-Cart"></i></a></li>
							</ul>
						</div>

						<!-- Section 2 -->
						<h3><span class="ui-accordion-header-icon ui-icon ui-accordion-icon"></span>2. Choose payment method</h3>
						<div style="border: 1px solid #d5d5d5;">
							<table style="margin: 0 auto;
							width: 50%;  ">
								<thead>
									<tr style="text-align: center;">
										<th>Online Payment</th>
									</tr>
								</thead>
								<tbody>
									<tr style="text-align: center;">
										<th><img src="./images/bg/stripe-payment-icon.png" alt=""></th>

									</tr>
									<tr>
										<th>
											<a href="" class="button" style="margin: 0 auto;
												 "> <i class="ln ln-icon-Money-2"></i> Payment with Stripe</a>
										</th>
									</tr>
								</tbody>
							</table>
						</div>



					</div>
					<!-- Accordion / End -->
				</div>


			</div>
			<!-- Container / End -->

			<br>
			<br>



		</div>
		<!-- Container / End -->



		<!-- Footer
================================================== -->
		<div class="margin-top-10"></div>

		<div id="footer">
			<!-- Main -->
			<div class="container">

				<div class="seven columns">
					<h4>About</h4>
					<p>Morbi convallis bibendum urna ut viverra. Maecenas quis consequat libero, a feugiat eros. Nunc ut lacinia
						tortor morbi ultricies laoreet ullamcorper phasellus semper.</p>
					<a href="#" class="button">Get Started</a>
				</div>

				<div class="three columns">
					<h4>Company</h4>
					<ul class="footer-links">
						<li><a href="#">About Us</a></li>
						<li><a href="#">Careers</a></li>
						<li><a href="#">Our Blog</a></li>
						<li><a href="#">Terms of Service</a></li>
						<li><a href="#">Privacy Policy</a></li>
						<li><a href="#">Hiring Hub</a></li>
					</ul>
				</div>

				<div class="three columns">
					<h4>Press</h4>
					<ul class="footer-links">
						<li><a href="#">In the News</a></li>
						<li><a href="#">Press Releases</a></li>
						<li><a href="#">Awards</a></li>
						<li><a href="#">Testimonials</a></li>
						<li><a href="#">Timeline</a></li>
					</ul>
				</div>

				<div class="three columns">
					<h4>Browse</h4>
					<ul class="footer-links">
						<li><a href="#">Freelancers by Category</a></li>
						<li><a href="#">Freelancers in USA</a></li>
						<li><a href="#">Freelancers in UK</a></li>
						<li><a href="#">Freelancers in Canada</a></li>
						<li><a href="#">Freelancers in Australia</a></li>
						<li><a href="#">Find Jobs</a></li>

					</ul>
				</div>

			</div>

			<!-- Bottom -->
			<div class="container">
				<div class="footer-bottom">
					<div class="sixteen columns">
						<h4>Follow Us</h4>
						<ul class="social-icons">
							<li><a class="facebook" href="#"><i class="icon-facebook"></i></a></li>
							<li><a class="twitter" href="#"><i class="icon-twitter"></i></a></li>
							<li><a class="gplus" href="#"><i class="icon-gplus"></i></a></li>
							<li><a class="linkedin" href="#"><i class="icon-linkedin"></i></a></li>
						</ul>
						<div class="copyrights">� Copyright 2018 by <a href="#"> Jobbee</a>. All Rights Reserved.</div>
					</div>
				</div>
			</div>

		</div>

		<!-- Back To Top Button -->
		<div id="backtotop"><a href="#"></a></div>

	</div>
	<!-- Wrapper / End -->