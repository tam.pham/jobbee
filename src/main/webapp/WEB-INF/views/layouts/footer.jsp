<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

		<!-- Footer
================================================== -->
			<div id="footer">
			<!-- Bottom -->
			<div class="container">
				<div class="footer-bottom">
					<div class="sixteen columns">
						<h4>Follow Us</h4>
						<ul class="social-icons">
							<li><a class="facebook" href="#"><i class="icon-facebook"></i></a></li>
							<li><a class="twitter" href="#"><i class="icon-twitter"></i></a></li>
							<li><a class="gplus" href="#"><i class="icon-gplus"></i></a></li>
							<li><a class="linkedin" href="#"><i class="icon-linkedin"></i></a></li>
						</ul>
						<div class="copyrights">� Copyright 2018 by <a href="#"> Jobbee</a>. All Rights Reserved.</div>
					</div>
				</div>
			</div>

		</div>

		<!-- Back To Top Button -->
		<div id="backtotop"><a href="#"></a></div>

	<!-- Wrapper / End -->


	<!-- Scripts
================================================== -->
	<!-- <script src="scripts/jquery-3.4.1.min.js"></script> -->
	<script src="scripts/jquery-migrate-3.1.0.min.js"></script>
	<script src="scripts/custom.js"></script>
	<script src="scripts/jquery.superfish.js"></script>
	<script src="scripts/jquery.themepunch.tools.min.js"></script>
	<script src="scripts/jquery.themepunch.revolution.min.js"></script>
	<script src="scripts/jquery.themepunch.showbizpro.min.js"></script>
	<script src="scripts/jquery.flexslider-min.js"></script>
	<script src="scripts/chosen.jquery.min.js"></script>
	<script src="scripts/jquery.magnific-popup.min.js"></script>
	<script src="scripts/waypoints.min.js"></script>
	<script src="scripts/jquery.counterup.min.js"></script>
	<script src="scripts/jquery.jpanelmenu.js"></script>
	<script src="scripts/stacktable.js"></script>
	<script src="scripts/slick.min.js"></script>
	<script src="scripts/headroom.min.js"></script>
	<script src="scripts/jquery-simple-validator.min.js"></script>
