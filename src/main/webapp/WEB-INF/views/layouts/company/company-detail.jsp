<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    

<!-- Content
================================================== -->
<div class="container">

	<!-- Recent Jobs -->
	<div class="eleven columns">
	<div class="padding-right">

		<!-- Company Info -->
		<div class="company-info">
			<img src="images/company-logo.png" alt="">
			<div class="content">
				<h4>${companyDetail.comName}</h4>
				<span><a href="#"><i class="fa fa-link"></i> ${companyDetail.website}</a></span>
			</div>
			<div class="clearfix"></div>
		</div>

		<p class="margin-reset">
            ${companyDetail.description}
		</p>
		
		

	</div>
	</div>


	<!-- Widgets -->
	<div class="five columns">

		<!-- Sort by -->
		<div class="widget">
			<h4>Overview</h4>

			<div class="job-overview">

				<ul>
					<li>
						<i class="fa fa-map-marker"></i>
						<div>
							<strong>Location:</strong>
							<span>${companyDetail.location}</span>
						</div>
					</li>
					<li>
						<i class="ln ln-icon-Globe"></i>
						<div>
							<strong>Website:</strong>
							<span>${companyDetail.website}</span>
						</div>
					</li>
					<li>
						<i class="ln ln-icon-Laptop-3"></i>
						<div>
							<strong>Bussiness Style:</strong>
							<span>${companyDetail.bussinessStyle}</span>
						</div>
					</li>
					<li>
						<i class="fa fa-user"></i>
						<div>
							<strong>People in there:</strong>
							<span>${companyDetail.size}+</span>
						</div>
					</li>
					
					
				</ul>

			</div>

		</div>

	</div>
	<!-- Widgets / End -->


</div>


<!-- Footer
================================================== -->
<div class="margin-top-50"></div>