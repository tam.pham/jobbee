<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

	<div id="wrapper">
		<!-- Header
================================================== -->
		<header class="dashboard-header">
			<div class="container">
				<div class="sixteen columns">

					<!-- Logo -->
					<div id="logo">
						<a href='<c:url value="/"/>' style="display: flex;
    align-items: flex-end;"><img src="images/bg/bee.png" style="width:50px; height:50px;" alt="  Jobbee" /><h1 style="margin-left:10px;">Jobbee</h1></a>
					</div>

					<!-- Menu -->
					<!-- <nav id="navigation" class="menu">
						<ul id="responsive">

							<li><a href="index-2.html">Home</a>
								<ul>
									<li><a href="index-2.html">Home #1</a></li>
									<li><a href="index-3.html">Home #2</a></li>
									<li><a href="index-4.html">Home #3</a></li>
									<li><a href="index-5.html">Home #4</a></li>
									<li><a href="index-6.html">Home #5</a></li>
								</ul>
							</li>

							<li><a href="#">Pages</a>
								<ul>
									<li><a href="job-page.html">Job Page</a></li>
									<li><a href="job-page-alt.html">Job Page Alternative</a></li>
									<li><a href="resume-page.html">Resume Page</a></li>
									<li><a href="shortcodes.html">Shortcodes</a></li>
									<li><a href="icons.html">Icons</a></li>
									<li><a href="pricing-tables.html">Pricing Tables</a></li>
									<li><a href="blog.html">Blog</a></li>
									<li><a href="contact.html">Contact</a></li>
								</ul>
							</li>

							<li><a href="#">Browse Listings</a>
								<ul>
									<li><a href="browse-jobs.html">Browse Jobs</a></li>
									<li><a href="browse-categories.html">Browse Categories</a></li>
									<li><a href="browse-resumes.html">Browse Resumes</a></li>
								</ul>
							</li>

							<li><a href="#" id="current">Dashboard</a>
								<ul>
									<li><a href="dashboard.html">Dashboard</a></li>
									<li><a href="dashboard-messages.html">Messages</a></li>
									<li><a href="dashboard-manage-resumes.html">Manage Resumes</a></li>
									<li><a href="dashboard-add-resume.html">Add Resume</a></li>
									<li><a href="dashboard-job-alerts.html">Job Alerts</a></li>
									<li><a href="dashboard-manage-jobs.html">Manage Jobs</a></li>
									<li><a href="dashboard-manage-applications.html">Manage Applications</a></li>
									<li><a href="dashboard-add-job.html">Add Job</a></li>
									<li><a href="dashboard-my-profile.html">My Profile</a></li>
								</ul>
							</li>
						</ul>


						<ul class="responsive float-right">
							<li><a href="dashboard.html"><i class="fa fa-cog"></i> Dashboard</a></li>
							<li><a href="index-2.html"><i class="fa fa-lock"></i> Log Out</a></li>
						</ul>

					</nav>

					Navigation
					<div id="mobile-navigation">
						<a href="#menu" class="menu-trigger"><i class="fa fa-reorder"></i></a>
					</div> -->

				</div>
			</div>
		</header>
		<div class="clearfix"></div>


		<!-- Titlebar
================================================== -->

		<!-- Dashboard -->
		<div id="dashboard">

			<!-- Navigation
	================================================== -->

			<!-- Responsive Navigation Trigger -->
			<a href="#" class="dashboard-responsive-nav-trigger"><i class="fa fa-reorder"></i> Dashboard Navigation</a>

				<div class="dashboard-nav">
				<div class="dashboard-nav-inner">

					<ul data-submenu-title="Management">
						<li class="active-submenu"><a>For Employers</a>
							<ul>
								<li><a href='<c:url value="/manage-post"/>'>Manage Jobs <span class="nav-tag">5</span></a></li>
								<li><a href='#'>Manage Applications <span class="nav-tag">4</span></a>
								<li><a href='<c:url value="/manage-company"/>'>Manage Company <span class="nav-tag">4</span></a></li>
								<li><a href='<c:url value="/manage-order"/>'>Manage Order <span class="nav-tag">4</span></a></li>
								<li><a href='<c:url value="/add-job"/>'>Add Job</a></li>
							</ul>
						</li>

					</ul>

					<ul data-submenu-title="Account">
						<li><a href='<c:url value="/employer-profile"/>'>My Profile</a></li>
						<li><a href="#">Logout</a></li>
					</ul>

				</div>
			</div>
			<!-- Navigation / End -->


			<!-- Content
	================================================== -->
			<div class="dashboard-content">

				<!-- Titlebar -->
				<div id="titlebar">
					<div class="row">
						<div class="col-md-12">
							<h2>Manage Company</h2>
							<!-- Breadcrumbs -->
							<nav id="breadcrumbs">
								<ul>
									<li><a href="">Home</a></li>
									<li><a href="#">Dashboard</a></li>
									<li>Manage Company</li>
								</ul>
							</nav>
						</div>
					</div>
				</div>


				<div class="row">

					<!-- Table-->
					<div class="col-lg-12 col-md-12">

						<div class="notification notice">
							Your listings are shown in the table below. Expired listings will be automatically removed after 30 days.
						</div>

						<div class="dashboard-list-box margin-top-30">
							<div class="dashboard-list-box-content">

								<!-- Table -->

								<table class="manage-table responsive-table">

									<tr>
										<th><i class="fa fa-file-text"></i> Company Name</th>
										<th><i class="fa fa-calendar"></i> Location</th>
										<th><i class="fa fa-calendar"></i> Size</th>
										<th><i class="fa fa-calendar"></i> Business Style</th>
										<th><i class="fa fa-user"></i> Website</th>
										<th>Action</th>
									</tr>

									<!-- Item #1 -->
									<c:forEach var="l" items="${companies}">
									<tr>
										
										<td class="title">${l.comName }</td>
										<td class="centered">${l.location}</td>
										<td>${l.size}+</td>
										<td>${l.bussinessStyle}</td>	
										<td>${l.website}</td>								
										<td class="action">
		                                    <a href="<c:url value="/update-company?id=${l.id}"/>"><i class="fa fa-pencil"></i> Edit</a>
											<a href="<c:url value="/delete-company?id=${l.id}"/>"></i> Delete</a>
										</td>
									</tr>
									</c:forEach>
									<!-- Item #2 -->
									<!-- <tr>
										<td class="title"><a href="#">Web Developer - Front End Web Development, Relational Databases</a>
										</td>
										<td class="centered">-</td>
										<td>September 30, 2015</td>
										<td>October 10, 2015</td>
										<td class="centered"><a href="dashboard-manage-applications.html" class="button">Show (4)</a></td>
										<td class="action">
											<a href="#"><i class="fa fa-pencil"></i> Edit</a>
											<a href="#" class="delete"><i class="fa fa-remove"></i> Delete</a>
										</td>
									</tr>

									Item #2
									<tr>
										<td class="title"><a href="#">Power Systems User Experience Designer</a></td>
										<td class="centered">-</td>
										<td>May 16, 2015</td>
										<td>June 30, 2015</td>
										<td class="centered"><a href="dashboard-manage-applications.html" class="button">Show (9)</a></td>
										<td class="action">
											<a href="#" class="delete"><i class="fa fa-remove"></i> Delete</a>
										</td>
									</tr>
 -->
								</table>

							</div>
						</div>
						<a href='<c:url value="/add-company"/>' class="button margin-top-30">Add New Company</a>
					</div>


					<!-- Copyrights -->
					<div class="col-md-12">
						<div class="copyrights">© 2019 Jobbee. All Rights Reserved.</div>
					</div>
				</div>

			</div>
			<!-- Content / End -->


		</div>
		<!-- Dashboard / End -->


	</div>
	<!-- Wrapper / End -->