<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

	<div id="wrapper">
		<!-- Header
================================================== -->
		<header class="dashboard-header">
			<div class="container">
				<div class="sixteen columns">

					<!-- Logo -->
					<div id="logo">
						<a href='<c:url value="/"/>' style="display: flex;
    align-items: flex-end;"><img src="images/bg/bee.png" style="width:50px; height:50px;" alt="  Jobbee" /><h1 style="margin-left:10px;">Jobbee</h1></a>
					</div>

					<!-- Menu -->
					<!-- <nav id="navigation" class="menu">
						<ul id="responsive">

							<li><a href="index-2.html">Home</a>
								<ul>
									<li><a href="index-2.html">Home #1</a></li>
									<li><a href="index-3.html">Home #2</a></li>
									<li><a href="index-4.html">Home #3</a></li>
									<li><a href="index-5.html">Home #4</a></li>
									<li><a href="index-6.html">Home #5</a></li>
								</ul>
							</li>

							<li><a href="#">Pages</a>
								<ul>
									<li><a href="job-page.html">Job Page</a></li>
									<li><a href="job-page-alt.html">Job Page Alternative</a></li>
									<li><a href="resume-page.html">Resume Page</a></li>
									<li><a href="shortcodes.html">Shortcodes</a></li>
									<li><a href="icons.html">Icons</a></li>
									<li><a href="pricing-tables.html">Pricing Tables</a></li>
									<li><a href="blog.html">Blog</a></li>
									<li><a href="contact.html">Contact</a></li>
								</ul>
							</li>

							<li><a href="#">Browse Listings</a>
								<ul>
									<li><a href="browse-jobs.html">Browse Jobs</a></li>
									<li><a href="browse-categories.html">Browse Categories</a></li>
									<li><a href="browse-resumes.html">Browse Resumes</a></li>
								</ul>
							</li>

							<li><a href="#" id="current">Dashboard</a>
								<ul>
									<li><a href="dashboard.html">Dashboard</a></li>
									<li><a href="dashboard-messages.html">Messages</a></li>
									<li><a href="dashboard-manage-resumes.html">Manage Resumes</a></li>
									<li><a href="dashboard-add-resume.html">Add Resume</a></li>
									<li><a href="dashboard-job-alerts.html">Job Alerts</a></li>
									<li><a href="dashboard-manage-jobs.html">Manage Jobs</a></li>
									<li><a href="dashboard-manage-applications.html">Manage Applications</a></li>
									<li><a href="dashboard-add-job.html">Add Job</a></li>
									<li><a href="dashboard-my-profile.html">My Profile</a></li>
								</ul>
							</li>
						</ul>


						<ul class="responsive float-right">
							<li><a href="dashboard.html"><i class="fa fa-cog"></i> Dashboard</a></li>
							<li><a href="index-2.html"><i class="fa fa-lock"></i> Log Out</a></li>
						</ul>

					</nav>

					Navigation
					<div id="mobile-navigation">
						<a href="#menu" class="menu-trigger"><i class="fa fa-reorder"></i></a>
					</div> -->

				</div>
			</div>
		</header>
		<div class="clearfix"></div>

	<!-- Dashboard -->
		<div id="dashboard">

			<!-- Navigation
	================================================== -->

			<!-- Responsive Navigation Trigger -->
			<a href="#" class="dashboard-responsive-nav-trigger"><i class="fa fa-reorder"></i> Dashboard Navigation</a>

				<div class="dashboard-nav">
				<div class="dashboard-nav-inner">

					<ul data-submenu-title="Management">
						<li class="active-submenu"><a>For Employers</a>
							<ul>
								<li><a href='<c:url value="/manage-post"/>'>Manage Jobs <span class="nav-tag">5</span></a></li>
								<li><a href='<c:url value="/manage-application"/>'>Manage Applications <span class="nav-tag">4</span></a>
								<li><a href='<c:url value="/manage-company"/>'>Manage Company <span class="nav-tag">4</span></a>
								</li>
								<li><a href='<c:url value="/add-job"/>'>Add Job</a></li>
							</ul>
						</li>

					</ul>

					<ul data-submenu-title="Account">
						<li><a href='<c:url value="/employer-profile"/>'>My Profile</a></li>
						<li><a href="#">Logout</a></li>
					</ul>

				</div>
			</div>
			<!-- Navigation / End -->


			<!-- Content
	================================================== -->
			<div class="dashboard-content">


				<!-- Titlebar -->
				<div id="titlebar">
					<div class="row">
						<div class="col-md-12">
							<h2>Add Company</h2>
							<!-- Breadcrumbs -->
							<nav id="breadcrumbs">
								<ul>
									<li><a href="#">Home</a></li>
									<li><a href="#">Dashboard</a></li>
									<li>Add Company</li>
								</ul>
							</nav>
						</div>
					</div>
				</div>

				<div class="row">

					<!-- Table-->
					<div class="col-lg-12 col-md-12">
					<form action="/add-company" method="POST" validate="true">
						<div class="dashboard-list-box margin-top-0">
							<h4>Company Details</h4>
							<div class="dashboard-list-box-content">
								<div class="submit-page">

									<!-- Company Name -->
									<div class="form">
										<h5>Company name</h5>
										<input class="search-field" type="text" placeholder="" value="" name="comName" maxlength="255" required minlength="5"/>
									</div>

									<!-- Website -->
									<div class="form">
										<h5>Website (Optional)</h5>
										<input class="search-field" type="text" placeholder="http://" value="" name="website"/>
									</div>

									<!-- Size -->
									<div class="form">
										<h5>Size</h5>
										<select data-placeholder="Choose size" class="chosen-select" name="size" required>
											<option value="50">50+</option>
											<option value="100">100+</option>
											<option value="200">200+</option>
											<option value="300">300+</option>
											<option value="400">400+</option>
											<option value="500">500+</option>
										</select>
									</div>

									<!-- Location -->
									<div class="form">
										<h5>Location</h5>
										<select data-placeholder="Choose Location" class="chosen-select" name="location" style="padding: 14px 18px;" required> 
												<c:forEach var="l" items="${locations}">
													<option value="${l.city}">${l.city}</option>
												</c:forEach>
										</select>
									</div>

									<div class="form">
										<div class="select">
											<h5>Business style</h5>
											<select data-placeholder="Choose Categories" class="chosen-select" name="bussinessStyle" style="padding: 14px 18px;" required> 
												<c:forEach var="l" items="${jobcategories}">
													<option value="${l.categoryName}">${l.categoryName}</option>
												</c:forEach>
											</select>
										</div>
									</div>

									<!-- Description -->
									<div class="form" style="width: 100%;">
										<h5>Description (Optional)</h5>
										<textarea name="description" class="WYSIWYG" cols="1" rows="1" id="summary" spellcheck="true"></textarea>
									</div>


								</div>

							</div>
						</div>

							<input type="submit" class="button border fw margin-top-10" name="addCompany" value="Add company" />
						</form>
					</div>

				</div>

			</div>
			<!-- Content / End -->


		</div>
		<!-- Dashboard / End -->