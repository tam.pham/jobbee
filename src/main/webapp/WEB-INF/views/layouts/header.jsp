<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


	<header class="sticky-header">
			<div class="container">
				<div class="sixteen columns">

					<!-- Logo -->
					<div id="logo" style="display:flex;justify-content: space-around;">
						<a href='<c:url value="/"/>' style="display:flex;align-items: flex-end;"><img alt="Jobbee"  src="../images/bg/bee.png" style="width:50px; height:50px;"/>
<h1 style="margin-left:10px;">Jobbee</h1></a>
					</div>

                    <!-- Menu -->
                    <nav id="navigation" class="menu">
                        <ul id="responsive">

                            <li><a  href='<c:url value="/list-job"/>'>Browse Jobs</a></li>

                            <li><a href='<c:url value="/list-company"/>'>Companies</a>
                            </li>

                            <li><a href='<c:url value="/blog"/>'>Blogs</a></li>
                        </ul>
                        <ul class="float-right">
                            <c:choose>
                                <c:when test="${userName != null}">
                                    <li><a href='#'><i style="color: #ec971f" class="fa fa-user"></i> ${userName}</a>
                                        <ul>
                                            <li style="border-bottom: 1px solid grey"><a href='<c:url value="/seeker-profile" />'>Dashboard</a></li>
                                            <li style="border-bottom: 1px solid grey"><a href='<c:url value="/seeker-profile" />'>Account Setting</a></li>
                                            <li id="btnLogout"><a href='#'>Logout</a></li>
                                        </ul>
                                    </li>
                                </c:when>
                                <c:otherwise>
                                    <li><a href='<c:url value="/login"/>'><i class="fa fa-user"></i> Login</a>

                                    </li>
                                </c:otherwise>
                            </c:choose>

                        </ul>

                    </nav>

				</div>
			</div>
		</header>	
		<div class="clearfix"></div>
<script src="scripts/jquery-3.4.1.min.js"></script>
<script src="scripts/bootstrap.min.js"></script>
=======
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


	<header class="sticky-header">
			<div class="container">
				<div class="sixteen columns">

					<!-- Logo -->
					<div id="logo" style="display:flex;justify-content: space-around;">
						<a href='<c:url value="/"/>' style="display:flex;align-items: flex-end;"><img alt="Jobbee"  src="../images/bg/bee.png" style="width:50px; height:50px;"/>
<h1 style="margin-left:10px;">Jobbee</h1></a>
					</div>

                    <!-- Menu -->
                    <nav id="navigation" class="menu">
                        <ul id="responsive">

                            <li><a  href='<c:url value="/list-job"/>'>Browse Jobs</a></li>

                            <li><a href='<c:url value="/list-company"/>'>Companies</a>
                            </li>

                            <li><a href='<c:url value="/blog"/>'>Blogs</a></li>
                            
                            <li><a href='<c:url value="/contact"/>'>Contact</a></li>
                        </ul>
                        <ul class="float-right">
                            <c:choose>
                                <c:when test="${userName != null}">
                                    <li><a href='#'><i style="color: #ec971f" class="fa fa-user"></i> ${userName}</a>
                                        <ul>
                                            <li style="border-bottom: 1px solid grey"><a href='<c:url value="/seeker-profile" />'>Dashboard</a></li>
                                            <li style="border-bottom: 1px solid grey"><a href='<c:url value="/seeker-profile" />'>Account Setting</a></li>
                                            <li id="btnLogout"><a href='#'>Logout</a></li>
                                        </ul>
                                    </li>
                                </c:when>
                                <c:otherwise>
                                    <li><a href='<c:url value="/login"/>'><i class="fa fa-user"></i> Login</a>

                                    </li>
                                </c:otherwise>
                            </c:choose>

                        </ul>

                    </nav>

				</div>
			</div>
		</header>	
		<div class="clearfix"></div>
<script src="scripts/jquery-3.4.1.min.js"></script>
<script src="scripts/bootstrap.min.js"></script>
<script src="scripts/ApiController.js"></script>